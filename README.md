﻿# BackgroundChanger (Title WIP)

![General Usage](https://bitbucket.org/eudoxsus/backgroundchanger/raw/2ea575d5ac962ab4b9c02b003c46b25f0ee0b8f2/demonstration.gif)

BackgroundChanger is an Android application that uses Facebook Graph API to set the device's background to randomly chosen images from a Facebook account. Reminisce with your or a loved one's albums!

 - Image Carousel main view for history-viewing and manual triggering of the next random image
 - User-defined constraints to random image selection
	 - Album Selection
	 - Date Range
 - Interface for cropping images to fit device screen

