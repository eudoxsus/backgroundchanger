package com.eudox.background.backgroundchanger

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.eudox.background.backgroundchanger.facebook.random.RandomPictureUtils
import com.eudox.background.backgroundchanger.facebook.random.Randomizer
import com.eudox.background.backgroundchanger.facebook.random.RandomizerImpl
import com.eudox.background.backgroundchanger.persist.*
import com.eudox.background.backgroundchanger.viewmodel.BackgroundWorkViewModel
import com.facebook.AccessToken
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class RoomTests {
    private lateinit var appDataDao: AppDataDao
    private lateinit var db: AppDatabase
    private lateinit var albumDao: AlbumDao
    private lateinit var randomizer: Randomizer

    private val testUntil: Long = 1449964800
    private val testSince: Long = 1449878400

    private val mobileUploadsId = "466301378837"

    private lateinit var backgroundViewModel: BackgroundWorkViewModel

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
                context, AppDatabase::class.java).build()
        appDataDao = db.appDataDao()
        albumDao = db.albumDao()
        randomizer = RandomizerImpl(context,db,false)
        //backgroundViewModel = BackgroundWorkViewModel(context as Application)

        runBlocking {
            albumDao.insert(Album(mobileUploadsId, AccessToken.getCurrentAccessToken().userId,true))
            appDataDao.insert(AppData(AppData.APP_ID_RAND, AppData.PAR_RAND_END_DTE,"1449964800"))
            appDataDao.insert(AppData(AppData.APP_ID_RAND, AppData.PAR_RAND_START_DTE,"1449878400"))
        }

    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun readTest(){
        runBlocking {
            val album = albumDao.getAlbumById(mobileUploadsId)
            assert(album?.id == mobileUploadsId)
        }
    }

    @Test
    @Throws(Exception::class)
    fun readLiveTest(){
        runBlocking {
            val randDates = appDataDao.getRandDatesLive()
            assert(randDates.value != null)
            randDates.value?.forEach {
                if (it.paramId == AppData.PAR_RAND_START_DTE){
                    val startDateLong = it.value.toLong()
                }
                else if (it.paramId == AppData.PAR_RAND_END_DTE){
                    val endDateLong = it.value.toLong()
                }
            }

        }
    }

    @Test
    @Throws(Exception::class)
    fun readNotLiveTest(){
        runBlocking {
            val randDates = appDataDao.getRandDates()
            randDates.forEach {
                if (it.paramId == AppData.PAR_RAND_START_DTE){
                    val startDateLong = it.value.toLong()
                }
                else if (it.paramId == AppData.PAR_RAND_END_DTE){
                    val endDateLong = it.value.toLong()
                }
            }

        }
    }

    @Test
    @Throws(Exception::class)
    fun randomTest() {
        runBlocking {
            val album = albumDao.getAlbumById(mobileUploadsId)
            RandomPictureUtils.requestAlbumBounded(album?.id,testSince,testUntil,randomizer)
        }
    }



    private fun insertRandDates(){
        //appDataDao.insert(AppData(AppData.APP_ID_RAND,AppData.PAR_RAND_START_DTE,AppData.PAR_RAND_END_DTE,""))
    }
}