package com.eudox.background.backgroundchanger.persist

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.eudox.background.backgroundchanger.adapter.AdapterAlbum

@Entity(tableName = Album.TABLE_NAME,primaryKeys = arrayOf(Album.ID_COL,Album.FB_USER_ID_COL))
data class Album(@ColumnInfo(name = ID_COL) val id: String, @ColumnInfo(name = FB_USER_ID_COL) val fb_user_id: String, val enabled: Boolean) {
    companion object{
        const val TABLE_NAME = "album_table"
        const val ID_COL = "album_id"
        const val FB_USER_ID_COL = "fb_user_id"
    }
}