package com.eudox.background.backgroundchanger.facebook.datasource.factory;

import com.eudox.background.backgroundchanger.facebook.datasource.SingleAlbumDataSource;
import com.eudox.background.backgroundchanger.facebook.model.PhotoData;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

public class SinglePhotoDataSourceFactory extends DataSource.Factory<String, PhotoData> {

    private MutableLiveData<PageKeyedDataSource<String, PhotoData>> singleAlbumLiveDataSource = new MutableLiveData<>();
    private String albumId;

    public SinglePhotoDataSourceFactory(String albumId){
        this.albumId = albumId;
    }

    @NonNull
    @Override
    public DataSource<String,PhotoData> create() {
        SingleAlbumDataSource singleAlbumDataSource = new SingleAlbumDataSource(albumId);
        singleAlbumLiveDataSource.postValue(singleAlbumDataSource);
        return singleAlbumDataSource;
    }

    public MutableLiveData<PageKeyedDataSource<String, PhotoData>> getSingleAlbumLiveDataSource() {
        return singleAlbumLiveDataSource;
    }
}
