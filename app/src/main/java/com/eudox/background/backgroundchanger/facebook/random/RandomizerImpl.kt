package com.eudox.background.backgroundchanger.facebook.random

import android.content.Context
import com.eudox.background.backgroundchanger.async.WallpaperSetter
import com.eudox.background.backgroundchanger.async.WallpaperSetterImpl
import com.eudox.background.backgroundchanger.facebook.model.AlbumJson
import com.eudox.background.backgroundchanger.facebook.model.PhotoData
import com.eudox.background.backgroundchanger.persist.*
import com.facebook.GraphResponse
import com.fasterxml.jackson.databind.ObjectMapper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.HashSet
import kotlin.random.Random

class RandomizerImpl(private val builder: Builder) : Randomizer {

    private val pictureDao: PictureDao
    private val appDataDao: AppDataDao
    private val albumDao: AlbumDao
    private var startDate: Long = 0
    private var endDate: Long = 0
    private var randomEpoch: Long? = null
    private val applicationContext: Context

    private var onCompleteListener: Randomizer.OnCompleteListener? = null
    private var isRestartable: Boolean = true
    private var isRepeatPhotosAccepted: Boolean = false
    private var setterFlag: WallpaperSetter.SetterFlag = WallpaperSetter.SetterFlag.BOTH

    init {
        pictureDao = builder.database.pictureDao()
        appDataDao = builder.database.appDataDao()
        albumDao = builder.database.albumDao()
        applicationContext = builder.applicationContext
        onCompleteListener = builder.OnCompleteListener
        isRestartable = builder.isRestartable
        isRepeatPhotosAccepted = builder.isRepeatPhotosAccepted
    }

    data class Builder(val applicationContext: Context,
                       val database: AppDatabase,
                       var isRestartable: Boolean = true,
                       var isRepeatPhotosAccepted: Boolean = false,
                       var OnCompleteListener: Randomizer.OnCompleteListener? = null){

        fun isRestartable(isRestartable: Boolean) = apply { this.isRestartable = isRestartable }
        fun isRepeatPhotosAccepted(isRepeatPhotosAccepted: Boolean) = apply { this.isRepeatPhotosAccepted = isRepeatPhotosAccepted }
        fun onCompleteListener(OnCompleteListener: Randomizer.OnCompleteListener) = apply { this.OnCompleteListener = OnCompleteListener }

        fun build() : Randomizer{
            return RandomizerImpl(this)
        }
    }

    override suspend fun randomWallpaperWork(): RandomizerResult {

        val startDb = appDataDao.getAppDataById(AppData.APP_ID_RAND, AppData.PAR_RAND_START_DTE)
        val endDb = appDataDao.getAppDataById(AppData.APP_ID_RAND, AppData.PAR_RAND_END_DTE)
        val setter = appDataDao.getAppDataById(AppData.APP_ID_WORK,AppData.PAR_WP_SETTER)

        if (startDb != null){
            startDate = startDb.value.toLong()
        }
        else{
            return RandomizerResult(RandomizerResult.Type.ERROR,"Start Date is null")
        }

        if (endDb != null){
            endDate = endDb.value.toLong()
        }
        else{
            return RandomizerResult(RandomizerResult.Type.ERROR,"End Date is null")
        }

        if (setter != null){
            setterFlag = WallpaperSetter.SetterFlag.valueOf(setter.value)
        }

        //TODO insert into a log database when error out
        //TODO switching to different album when out of pictures
        val randomEpoch: Long = RandomPictureUtils.getRandomDate(startDate, endDate)
        val randomPlusOne: Long = randomEpoch + RandomPictureUtils.getOneDayEpoch()

        this.randomEpoch = randomEpoch
        RandomPictureUtils.requestAlbumBounded(albumDao.getRandomAlbum().id, randomEpoch, randomPlusOne,this)
        return RandomizerResult(RandomizerResult.Type.SUCCESS,"Request Sent")
    }

    override fun onCompleted(response: GraphResponse?) {
        //format JSON
        val albumJson: AlbumJson = ObjectMapper().readValue(response?.rawResponse, AlbumJson::class.java)
        CoroutineScope(Dispatchers.Default).launch {
            var findNewDate = false
            lateinit var randPhotoData: PhotoData
            var picture: Picture? = null
            val consumedInds: MutableSet<Int> = HashSet()

            do {
                //If all the pictures in this album have been used then find a new date to use
                if (albumJson.photos == null || consumedInds.size == albumJson.photos.data.size){
                    findNewDate = true
                    break
                }
                //Find a random index
                var randInd: Int = Random.nextInt(albumJson.photos.data.size)
                // If the index was used before, find a new one
                while (consumedInds.contains(randInd)) {
                    randInd = Random.nextInt(albumJson.photos.data.size)
                }
                randPhotoData = albumJson.photos.data[randInd]
                //Check if this picture is in the db
                picture = pictureDao.getPictureByPictureId(randPhotoData.id)
                consumedInds.add(randInd)
            } while ((!isRepeatPhotosAccepted && picture != null) || (picture != null && !picture.enabled))

            if (findNewDate){
                //Then we need to restart the work for a new date
                if (isRestartable){
                    randomWallpaperWork()
                }
            }else{
                //We have found a picture and now will use it
                val wallpaperSetter = WallpaperSetterImpl(applicationContext)
                if (picture != null){
                    wallpaperSetter.setWallpaper(picture,setterFlag)
                }
                else{
                    wallpaperSetter.setWallpaper(Picture(randPhotoData.id,albumJson.id,randPhotoData.source,true,false,Date(),Date()),setterFlag)
                }
                onCompleteListener?.onComplete()

            }
        }
    }
}