package com.eudox.background.backgroundchanger.fragment

import android.app.DatePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.work.WorkInfo
import androidx.work.WorkManager
import com.eudox.background.backgroundchanger.fragment.dialog.DatePickerDialogFragment
import com.eudox.background.backgroundchanger.R
import com.eudox.background.backgroundchanger.fragment.dialog.TimePeriodDialogFragment
import com.eudox.background.backgroundchanger.background.WallpaperWorker
import com.eudox.background.backgroundchanger.persist.AppData
import com.eudox.background.backgroundchanger.persist.AppDataRepository
import com.eudox.background.backgroundchanger.viewmodel.BackgroundWorkViewModel
import kotlinx.android.synthetic.main.date_settings_cell.*
import kotlinx.android.synthetic.main.fragment_background_work2.*
import kotlinx.android.synthetic.main.work_period_settings_cell.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*


class BackgroundWorkFragment : Fragment(), DatePickerDialogFragment.RandomDateDialogListenerProvider, TimePeriodDialogFragment.InitTimeViewProvider, TimePeriodDialogFragment.TimePeriodDialogListenerProvider {

    private lateinit var workManager : WorkManager
    private lateinit var viewModel: BackgroundWorkViewModel
    private lateinit var dateFormat: SimpleDateFormat

    companion object{
        const val DATE_FORMAT_STR = "E, dd MMM yyyy HH:mm:ss z"
    }

    private fun initialize(context: Context){

        workPeriodCardView.setOnClickListener {
            timePeriodDialogDisplay()
        }

        startDateText.inputType = InputType.TYPE_NULL
        endDateText.inputType = InputType.TYPE_NULL

        startDateText.setOnClickListener(View.OnClickListener {
            dateDialogDisplay(AppData.PAR_RAND_START_DTE,viewModel.startDateLong)
        })
        startDateText.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                dateDialogDisplay(AppData.PAR_RAND_START_DTE, viewModel.startDateLong)
            }
        }
        endDateText.setOnClickListener(View.OnClickListener {
            dateDialogDisplay(AppData.PAR_RAND_END_DTE,viewModel.endDateLong)
        })
        endDateText.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                dateDialogDisplay(AppData.PAR_RAND_END_DTE, viewModel.endDateLong)
            }
        }

        viewModel.randDatesLive.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            //Set Dates
            viewModel.viewModelScope.launch(Dispatchers.IO){
                val datePair = viewModel.observeRandDatesLive(it)
                withContext(Dispatchers.Main){
                    startDateText.text = datePair.first
                    endDateText.text = datePair.second
                }
            }
        })

        viewModel.timeDataLive.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            viewModel.observeTimePeriodDataLive(it)
            timeAmount.text = viewModel.periodValue.toString()
            val tuStr = viewModel.timeUnit.toString().toLowerCase(Locale.ROOT)
            timeUnit.text = tuStr.capitalize(Locale.ROOT)
            setWorkDateText()
        })



        val workLiveData = WorkManager.getInstance(requireContext()).getWorkInfosForUniqueWorkLiveData(WallpaperWorker.RANDOM_WORK_NAME)

        workLiveData.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            if (it.isNullOrEmpty() || (it[0].state != WorkInfo.State.ENQUEUED && it[0].state != WorkInfo.State.RUNNING)){
                enable_sw.isChecked = false
                enable_sw.text = "Disabled"
                switchText.text = ""
                switchText.visibility = View.INVISIBLE
                return@Observer
            }
            else{
                enable_sw.isChecked = true
                enable_sw.text = "Enabled"
            }
        })

        enable_sw.setOnClickListener {
            if (enable_sw.isChecked){
                val canEnablePair = viewModel.canEnableWork()
                if (canEnablePair.first){
                    viewModel.enqueueWorkPeriodic(requireContext())
                    Toast.makeText(requireContext(),"New wallpaper scheduled every ${viewModel.periodValue} ${viewModel.timeUnit.toString().toLowerCase(Locale.ROOT).capitalize(Locale.ROOT)}",Toast.LENGTH_LONG).show()
                }
                else{
                    enable_sw.isChecked = false
                    Toast.makeText(requireContext(),canEnablePair.second,Toast.LENGTH_LONG).show()
                    //TODO Show dialog for error
                }
            }
            else{
                viewModel.dequeueWorkPeriodic(requireContext())
            }
        }
    }

    private fun setWorkDateText(){
        switchText.visibility = View.VISIBLE
        val dateObj = Date(viewModel.nextWorkDateLong)
        switchText.text = "Next change:\n ${dateFormat.format(dateObj)}"
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_background_work2, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(BackgroundWorkViewModel::class.java)
        workManager = WorkManager.getInstance(requireContext())
        dateFormat = SimpleDateFormat(DATE_FORMAT_STR)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        context?.let { initialize(it) }
    }

    private fun dateDialogDisplay(mode : String, dateLong: Long? = null){
        if (dateLong != null){
            val newFragment = DatePickerDialogFragment()
            val args: Bundle = Bundle()
            args.putString(DatePickerDialogFragment.LISTENER_SELECT_KEY,mode)
            args.putLong(DatePickerDialogFragment.INIT_DATE_KEY,dateLong * 1000)
            newFragment.arguments = args
            newFragment.setTargetFragment(this,1)
            newFragment.show(parentFragmentManager , "datePicker")
            when(mode){
                AppData.PAR_RAND_START_DTE ->{
                    Toast.makeText(requireContext(),"Set the start date to randomly set the wallpaper",Toast.LENGTH_LONG).show()
                }
                AppData.PAR_RAND_END_DTE ->{
                    Toast.makeText(requireContext(),"Set the end date to randomly set the wallpaper",Toast.LENGTH_LONG).show()
                }
            }
        }
        else{
            val newFragment = DatePickerDialogFragment()
            newFragment.setTargetFragment(this,1)
            newFragment.show(parentFragmentManager ,"datePicker")
        }
    }

    private fun timePeriodDialogDisplay(){
        val dialogFragment = TimePeriodDialogFragment()
        dialogFragment.setTargetFragment(this,1)
        dialogFragment.show(parentFragmentManager ,"timePicker")
    }

    override fun getOnStartDateSetListener(): DatePickerDialog.OnDateSetListener {
        //Set StartDateText Listeners
        return DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
            CoroutineScope(Dispatchers.IO).launch {
                val msg = viewModel.submitDatePickerDialogResult(year,month, dayOfMonth, AppDataRepository.AppDataSubmitEnum.RAND_START_DTE)
                withContext(Dispatchers.Main){
                    Toast.makeText(requireContext(),msg,Toast.LENGTH_SHORT).show()
                }
            }
            startDateText.text = viewModel.formatDatePickerValToStr(year,month,dayOfMonth)
        }
    }

    override fun getOnEndDateSetListener(): DatePickerDialog.OnDateSetListener {
        //Set EndDateText Listeners
        return DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
            CoroutineScope(Dispatchers.IO).launch {
                val msg = viewModel.submitDatePickerDialogResult(year,month, dayOfMonth,AppDataRepository.AppDataSubmitEnum.RAND_END_DTE)
                withContext(Dispatchers.Main){
                    Toast.makeText(requireContext(),msg,Toast.LENGTH_SHORT).show()
                }
            }
            endDateText.text = viewModel.formatDatePickerValToStr(year,month,dayOfMonth)
        }
    }

    override fun getTimeDialogInit(): (timeView: View) -> Unit {
        return viewModel.getTimeDialogInit()
    }

    override fun getOnPositiveButtonClickListener(): (dialog: DialogInterface, timeView: View) -> Unit {
        return viewModel.getOnPositiveButtonClickListener()
    }

    override fun getOnNegativeButtonClickListener(): (dialog: DialogInterface, timeView: View) -> Unit {
        return viewModel.getOnNegativeButtonClickListener()
    }
}