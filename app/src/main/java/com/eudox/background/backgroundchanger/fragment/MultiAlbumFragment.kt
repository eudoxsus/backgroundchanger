package com.eudox.background.backgroundchanger.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eudox.background.backgroundchanger.R
import com.eudox.background.backgroundchanger.adapter.AdapterAlbumDiffCallback
import com.eudox.background.backgroundchanger.adapter.MultiAlbumAdapterK
import com.eudox.background.backgroundchanger.viewmodel.MultiAlbumViewModel

class MultiAlbumFragment : Fragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val gridRecycler: RecyclerView = view.findViewById(R.id.gridRecycler)
        gridRecycler.layoutManager = LinearLayoutManager(context)
        val multiAlbumViewModel: MultiAlbumViewModel = ViewModelProvider(this).get(MultiAlbumViewModel::class.java)
        val adapter = MultiAlbumAdapterK(requireContext(),multiAlbumViewModel,multiAlbumViewModel,multiAlbumViewModel,AdapterAlbumDiffCallback())

        /*
        multiAlbumViewModel.allAlbums.observe(viewLifecycleOwner, Observer<List<Album?>?> {

        })
        */


        multiAlbumViewModel.albumPagedList.observe(viewLifecycleOwner, { albums ->
            adapter.submitList(albums)
        })
        gridRecycler.adapter = adapter
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_multi_album, container, false)
    }
}