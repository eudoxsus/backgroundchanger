package com.eudox.background.backgroundchanger.async.deprecated;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class BitmapDecodeRunnable implements Runnable {

    private URL url;
    private BitmapTaskHandler handler;
    private File tempDir;

    public BitmapDecodeRunnable(BitmapTaskHandler handler,File tempDir,String source){
        try {
            url = new URL(source);
            this.handler = handler;
            this.tempDir = tempDir;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            File temp = File.createTempFile("img", BitmapTaskHandler.IMG_EXT,tempDir);
            OutputStream outStream = null;
            Bitmap bmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            outStream = new FileOutputStream(temp);
            bmap.compress(Bitmap.CompressFormat.PNG, 85, outStream);
            outStream.close();
            handler.setImgFile(temp);
            handler.handleState(BitmapTaskHandler.DECODE_STATE_COMPLETED);
//            DisplayMetrics metrics = new DisplayMetrics();
//            windowManager.getDefaultDisplay().getMetrics(metrics);
//            int height = metrics.heightPixels;
//            int width = metrics.widthPixels;
//            Bitmap bitmap = Bitmap.createScaledBitmap(bmap, width, height, true);
//            wallpaperManager.setBitmap(bitmap);
        } catch (IOException e) {
            handler.handleState(BitmapTaskHandler.RUNNABLE_ERR);
            e.printStackTrace();
        }
    }
}
