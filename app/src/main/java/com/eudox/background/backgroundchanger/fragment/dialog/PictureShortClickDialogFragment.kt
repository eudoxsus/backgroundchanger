package com.eudox.background.backgroundchanger.fragment.dialog

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.eudox.background.backgroundchanger.R

class PictureShortClickDialogFragment : DialogFragment() {
    companion object{
        const val PHOTO_POSITION = "PHOTO_POS"
    }

    private lateinit var dialogOnClickListener: DialogInterface.OnClickListener

    interface DialogOnClickListenerProvider{
        fun getOnClickListener(clickedPosition: Int) : DialogInterface.OnClickListener
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val dialogOnClickListener = targetFragment as? DialogOnClickListenerProvider
        val position = arguments?.getInt(PHOTO_POSITION)

        if (dialogOnClickListener != null && position != null){
            this.dialogOnClickListener = dialogOnClickListener.getOnClickListener(position)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        return activity?.let {
            // Use the Builder class for convenient dialog construction
            val builder = AlertDialog.Builder(it)

            builder
                    .setTitle(R.string.picture_short_click_dialog_title)
                    .setItems(R.array.picture_short_click_array,dialogOnClickListener)
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}