package com.eudox.background.backgroundchanger.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eudox.background.backgroundchanger.R
import com.eudox.background.backgroundchanger.viewmodel.enums.SingleAlbumModeEnum

class SingleAlbumAdapter2<T : AdapterPhoto>(private val context: Context, diffCallback: DiffUtil.ItemCallback<T>,private val pictureClickListener: View.OnClickListener? = null) : PagedListAdapter<T, SingleAlbumAdapter2<T>.ViewHolder>(diffCallback) {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val imageView: ImageView = itemView.findViewById(R.id.cardImage)

        fun bindTo(photoData: T) {
            Glide.with(context)
                    .load(photoData.source)
                    .into(imageView)
            itemView.setOnClickListener {

            }
            /*
            itemView.setOnClickListener(onClickListener)

            File cacheDir = context.getCacheDir();
            BitmapTaskHandler taskHandler = new BitmapTaskHandlerImpl(context);
            Runnable runnable = new BitmapDecodeRunnable(taskHandler,cacheDir,photoData.getSource());
            taskHandler.submitTask(executor,runnable);
            */
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.img_list_cell, parent, false)
        return this.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val photoData: T? = getItem(position)
        if (photoData != null) {
            holder.bindTo(photoData)
        } else {
            Toast.makeText(context, "Item is null", Toast.LENGTH_LONG).show()
        }
    }

}