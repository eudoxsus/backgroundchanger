package com.eudox.background.backgroundchanger.viewmodel.factory;

import android.app.Application;

import com.eudox.background.backgroundchanger.viewmodel.HistoryViewModel;
import com.eudox.background.backgroundchanger.viewmodel.SingleAlbumViewModel;
import com.eudox.background.backgroundchanger.viewmodel.enums.SingleAlbumModeEnum;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class PictureCollectionViewModelFactory implements ViewModelProvider.Factory {

    private String albumId;
    private Application application;
    private SingleAlbumModeEnum modeEnum;

    public PictureCollectionViewModelFactory(Application application, SingleAlbumModeEnum modeEnum,  String albumId){
        this.albumId = albumId;
        this.modeEnum = modeEnum;
        this.application = application;
    }

    public PictureCollectionViewModelFactory(Application application, SingleAlbumModeEnum modeEnum){
        this.modeEnum = modeEnum;
    }


    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        switch (modeEnum){
            case ALBUM:
                return (T) new SingleAlbumViewModel(application,albumId);
            case HISTORY:
                return (T) new HistoryViewModel(application);
            default:
                return (T) new HistoryViewModel(application);
        }
    }
}
