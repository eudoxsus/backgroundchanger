package com.eudox.background.backgroundchanger.fragment

import android.content.DialogInterface
import android.net.Uri
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.eudox.background.backgroundchanger.R
import com.eudox.background.backgroundchanger.async.WallpaperSetter
import com.eudox.background.backgroundchanger.async.WallpaperSetterImpl
import com.eudox.background.backgroundchanger.facebook.random.Randomizer
import com.eudox.background.backgroundchanger.facebook.random.RandomizerImpl
import com.eudox.background.backgroundchanger.fragment.dialog.PictureShortClickDialogFragment
import com.eudox.background.backgroundchanger.persist.AppDatabase
import com.eudox.background.backgroundchanger.persist.Picture
import com.eudox.background.backgroundchanger.viewmodel.SelectViewModel
import com.eudox.background.backgroundchanger.viewmodel.enums.SingleAlbumModeEnum
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.fragment_select.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.imaginativeworld.whynotimagecarousel.CarouselItem
import org.imaginativeworld.whynotimagecarousel.CarouselOnScrollListener
import org.imaginativeworld.whynotimagecarousel.OnItemClickListener
import java.util.concurrent.atomic.AtomicBoolean

class SelectFragment : Fragment(), PictureShortClickDialogFragment.DialogOnClickListenerProvider {

    lateinit var viewModel: SelectViewModel
    private lateinit var carouselPictures: List<Picture>
    private lateinit var wallpaperSetter: WallpaperSetter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(SelectViewModel::class.java)
        wallpaperSetter = WallpaperSetterImpl(requireContext())
    }

    /**
     * Disables the left scroll random action when async work is ongoing
     */
    private var randomActionEnable: Boolean = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_select, container, false)
    }

    private fun pictureShortClickDialogDisplay(position: Int){
        val newFragment = PictureShortClickDialogFragment()
        val args: Bundle = Bundle()
        args.putInt(PictureShortClickDialogFragment.PHOTO_POSITION, position)
        newFragment.arguments = args
        newFragment.setTargetFragment(this, 1)
        newFragment.show(parentFragmentManager, "pictureShortClick")
    }

    private fun startImageCropperActivity(carouselItem: CarouselItem){
        /*
         * Passes image to intent
         */
        val windowManager: WindowManager = (requireActivity() as AppCompatActivity).windowManager
        val metrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(metrics)
        val height = metrics.heightPixels
        val width = metrics.widthPixels

        //TODO Passing the result through the activity pipeline correctly
        CropImage.activity(Uri.parse(carouselItem.imageUrl)).setAutoZoomEnabled(true).setInitialCropWindowPaddingRatio(0f).setFixAspectRatio(true).setAspectRatio(width, height).start(requireContext(), this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //TODO Add Like and Dislike buttons and functionality

        carousel.onItemClickListener = object : OnItemClickListener{
            override fun onClick(position: Int, carouselItem: CarouselItem) {
                pictureShortClickDialogDisplay(position)
            }

            override fun onLongClick(position: Int, dataObject: CarouselItem) {
                /*Dialog menu
                Bring up OnClick Menu
                Resize image, even if not current. TODO storing the dimensions selected
                delete from history
                 */
            }
        }

        carousel.onScrollListener = object : CarouselOnScrollListener {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int, position: Int, carouselItem: CarouselItem?) {
                when(newState){
                    RecyclerView.SCROLL_STATE_SETTLING -> {
                        if (randomActionEnable && !recyclerView.canScrollHorizontally(-1)) {
                            randomActionEnable = false
                            //Unlocks the action on completion of randomWork
                            val onCompletedListener: Randomizer.OnCompleteListener = Randomizer.OnCompleteListener {
                                randomActionEnable = true
                            }

                            val randomizerBuilder: RandomizerImpl.Builder = RandomizerImpl.Builder(requireContext(), AppDatabase.getDatabase(requireContext())).onCompleteListener(onCompletedListener)
                            val randomizer: Randomizer = RandomizerImpl(randomizerBuilder)
                            Toast.makeText(requireContext(), "Selecting random image...", Toast.LENGTH_SHORT).show()
                            CoroutineScope(Dispatchers.Default).launch {
                                randomizer.randomWallpaperWork()
                            }
                        }
                        if (!recyclerView.canScrollHorizontally(1)) {
                            val nav = SelectFragmentDirections.actionSelectFragmentToHistory(null,SingleAlbumModeEnum.HISTORY)
                            Navigation.findNavController(recyclerView).navigate(nav)
                        }
                    }
                }
            }
        }

        viewModel.carouselItemLiveDataList.observe(viewLifecycleOwner, Observer { carouselItems ->
            carousel.addData(carouselItems)
        })

        viewModel.pictureLiveDataList.observe(viewLifecycleOwner, Observer { pictures ->
            carouselPictures = pictures
        })
    }

    override fun getOnClickListener(clickedPosition: Int): DialogInterface.OnClickListener {
        return DialogInterface.OnClickListener { dialog, which ->
            val picture = carouselPictures[clickedPosition]

            CoroutineScope(Dispatchers.Default).launch {
                val options = resources.getStringArray(R.array.picture_short_click_array)
                when(resources.getStringArray(R.array.picture_short_click_array)[which]){
                    //System
                    options[0] -> {
                        wallpaperSetter.setWallpaper(picture, WallpaperSetter.SetterFlag.SYSTEM)
                    }
                    //Lock
                    options[1] -> {
                        wallpaperSetter.setWallpaper(picture, WallpaperSetter.SetterFlag.LOCK)
                    }
                    //Both
                    options[2] -> {
                        wallpaperSetter.setWallpaper(picture, WallpaperSetter.SetterFlag.BOTH)
                    }
                }
            }
        }
    }

}