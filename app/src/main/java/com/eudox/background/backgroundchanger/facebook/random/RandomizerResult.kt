package com.eudox.background.backgroundchanger.facebook.random

class RandomizerResult(val type: Type,val msg : String?) {
    enum class Type {
        SUCCESS,
        ERROR
    }
}