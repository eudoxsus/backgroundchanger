package com.eudox.background.backgroundchanger.async

import com.eudox.background.backgroundchanger.persist.Picture

interface WallpaperSetter {
    suspend fun setWallpaper(picture: Picture,setterFlag: SetterFlag)

    enum class SetterFlag {
        SYSTEM,
        LOCK,
        BOTH
    }
}