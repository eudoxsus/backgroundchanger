package com.eudox.background.backgroundchanger.background

import android.content.Context
import androidx.work.*
import com.eudox.background.backgroundchanger.async.WallpaperSetter
import com.eudox.background.backgroundchanger.facebook.random.Randomizer
import com.eudox.background.backgroundchanger.facebook.random.RandomizerImpl
import com.eudox.background.backgroundchanger.persist.*
import java.util.*
import java.util.concurrent.TimeUnit

class WallpaperWorker(private val appContext: Context, workerParams: WorkerParameters) : CoroutineWorker(appContext, workerParams) {

    companion object{
        const val RANDOM_WORK_NAME: String = "random_pic"
        fun build(duration: Long,timeUnit: TimeUnit) : PeriodicWorkRequest {
            return  PeriodicWorkRequestBuilder<WallpaperWorker>(duration, timeUnit).setInitialDelay(duration, timeUnit)
                    .setConstraints(buildConstraints()).build()
        }
        private fun buildConstraints() : Constraints{
            return Constraints.Builder().setRequiresBatteryNotLow(true).setRequiredNetworkType(NetworkType.UNMETERED)
                    .setRequiresStorageNotLow(true).build()
        }
    }

    override suspend fun doWork(): Result {
        val db = AppDatabase.getDatabase(appContext)
        val appDataRepo = AppDataRepository.getInstance(appContext)
        val builder: RandomizerImpl.Builder = RandomizerImpl.Builder(appContext,db)
        val randomizer: Randomizer = RandomizerImpl(builder)
        randomizer.randomWallpaperWork()

        val timeUnit: TimeUnit? = appDataRepo?.getAppDataById(AppData.APP_ID_WORK,AppData.PAR_WP_TU)?.value?.let { TimeUnit.valueOf(it.toUpperCase(Locale.ROOT)) }
        val periodVal: String? = appDataRepo?.getAppDataById(AppData.APP_ID_WORK,AppData.PAR_WP_TIME_VAL)?.value


        if (timeUnit != null && periodVal != null){
            val dateObj = Date()
            appDataRepo.handleSubmit((dateObj.time + TimeUnit.MILLISECONDS.convert(periodVal.toLong(),timeUnit)).toString(),AppDataRepository.AppDataSubmitEnum.WP_NEXT_WORK_DTE)
        }


        /*
        val randomEpoch: Long = RandomPictureUtils.getRandomDate(startDate, endDate)

        albumId = album.id
        this.randomEpoch = randomEpoch
        RandomPictureUtils.requestAlbumBounded(album.id, randomEpoch, randomEpoch,this)*/

        /*
        1. Find a random album that is enabled - launch x
        2. Get the bound dates - launch x
        2a. Get a random date with the bound dates x
        3. Make facebook Request to get the pictures - await x
        4. Make random number to choose from the array of pictures in request x
        5. Check if id is not enabled in history db - async
        5a. If is, increment a retry variable to keep track of retries in this array x
        5b. restart from step 4
        5c. If retry variable reaches array size then select a different date, blacklist the date
        6. If good, then send the link to the wallpaper setter to set the wallpaper - launch
        7. Insert into pictures history database
         */

        return Result.success()
    }

}