package com.eudox.background.backgroundchanger.async

import android.app.WallpaperManager
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.eudox.background.backgroundchanger.async.deprecated.BitmapTaskHandler
import com.eudox.background.backgroundchanger.persist.AppDatabase
import com.eudox.background.backgroundchanger.persist.Picture
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.net.URL
import java.util.*

class WallpaperSetterImpl(val context: Context) : WallpaperSetter {

    private var wallpaperManager : WallpaperManager = WallpaperManager.getInstance(context)
    private val cacheDir = context.cacheDir
    private val pictureDao = AppDatabase.getDatabase(context).pictureDao()

    override suspend fun setWallpaper(picture: Picture, setterFlag: WallpaperSetter.SetterFlag) {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val url = URL(picture.source)

                val temp = File.createTempFile("img", BitmapTaskHandler.IMG_EXT, cacheDir)
                var outStream: OutputStream? = null
                val bmap = BitmapFactory.decodeStream(url.openConnection().getInputStream())
                outStream = FileOutputStream(temp)
                bmap.compress(Bitmap.CompressFormat.PNG, 85, outStream)
                outStream.close()

                val bm = BitmapFactory.decodeFile(temp.path)
                when(setterFlag){
                    WallpaperSetter.SetterFlag.SYSTEM -> wallpaperManager.setBitmap(bm,null,false,WallpaperManager.FLAG_SYSTEM)
                    WallpaperSetter.SetterFlag.LOCK -> wallpaperManager.setBitmap(bm,null,false,WallpaperManager.FLAG_LOCK)
                    WallpaperSetter.SetterFlag.BOTH -> wallpaperManager.setBitmap(bm)
                }
                val dbPic = pictureDao.getPictureByFullId(picture.id,picture.album_id)

                if (dbPic != null){
                    pictureDao.update(Picture(picture.id,picture.album_id,picture.source,picture.enabled,picture.favorite,picture.create_date, Date()))
                }
                else{
                    pictureDao.insert(Picture(picture.id,picture.album_id,picture.source,true,false, Date(), Date()))
                }

            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

}