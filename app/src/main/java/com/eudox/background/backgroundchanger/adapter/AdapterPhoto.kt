package com.eudox.background.backgroundchanger.adapter

/**
 * Interface to implement that gives an adapter enough information to display and differentiate different photos
 */
interface AdapterPhoto {
    val source: String
    val id: String

    override fun equals(other: Any?): Boolean
}