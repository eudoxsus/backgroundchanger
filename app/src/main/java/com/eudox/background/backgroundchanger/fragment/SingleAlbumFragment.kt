package com.eudox.background.backgroundchanger.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.GridLayoutManager
import com.eudox.background.backgroundchanger.R
import com.eudox.background.backgroundchanger.fragment.SingleAlbumFragmentArgs
import com.eudox.background.backgroundchanger.adapter.AdapterPhotoDiffCallback
import com.eudox.background.backgroundchanger.adapter.SingleAlbumAdapter2
import com.eudox.background.backgroundchanger.facebook.model.PhotoData
import com.eudox.background.backgroundchanger.persist.Picture
import com.eudox.background.backgroundchanger.viewmodel.HistoryViewModel
import com.eudox.background.backgroundchanger.viewmodel.SingleAlbumViewModel
import com.eudox.background.backgroundchanger.viewmodel.enums.SingleAlbumModeEnum
import com.eudox.background.backgroundchanger.viewmodel.factory.PictureCollectionViewModelFactory
import kotlinx.android.synthetic.main.fragment_single_album.*
import kotlinx.coroutines.launch

class SingleAlbumFragment : Fragment() {

    private val DP_WID = 100

    private fun calculateNoOfColumns(context: Context, columnWidthDp: Float): Int { // For example columnWidthdp=180
        val displayMetrics = context.resources.displayMetrics
        val screenWidthDp = displayMetrics.widthPixels / displayMetrics.density
        return (screenWidthDp / columnWidthDp + 0.5).toInt()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_single_album, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        gridRecycler.setLayoutManager(GridLayoutManager(context, calculateNoOfColumns(requireContext(), DP_WID.toFloat())))

        val modeEnum = arguments?.let { SingleAlbumFragmentArgs.fromBundle(it).mode }
        val albumKey = arguments?.let { SingleAlbumFragmentArgs.fromBundle(it).albumId }

        when(modeEnum){
             SingleAlbumModeEnum.ALBUM -> {
                 val adapter = SingleAlbumAdapter2(requireContext(),AdapterPhotoDiffCallback<PhotoData>())
                 gridRecycler.adapter = adapter
                val singleAlbumViewModel: SingleAlbumViewModel = ViewModelProvider(this, PictureCollectionViewModelFactory(requireActivity().application,modeEnum)).get(SingleAlbumViewModel::class.java)
                singleAlbumViewModel.photoDataPagedList.observe(viewLifecycleOwner, { photos -> adapter.submitList(photos) })
            }
            SingleAlbumModeEnum.HISTORY ->{
                val adapter = SingleAlbumAdapter2(requireContext(),AdapterPhotoDiffCallback<Picture>())
                gridRecycler.adapter = adapter
                val historyViewModel: HistoryViewModel = ViewModelProvider(this, PictureCollectionViewModelFactory(requireActivity().application, modeEnum, albumKey)).get(HistoryViewModel::class.java)
                historyViewModel.viewModelScope.launch {
                    historyViewModel.getPicturePagedList().observe(viewLifecycleOwner, { pictures ->
                        adapter.submitList(pictures)
                    })
                }
            }
        }



    }
}