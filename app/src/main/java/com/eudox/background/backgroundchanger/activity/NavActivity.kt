package com.eudox.background.backgroundchanger.activity

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.eudox.background.backgroundchanger.R
import com.eudox.background.backgroundchanger.fragment.SelectFragmentDirections
import com.eudox.background.backgroundchanger.viewmodel.enums.SingleAlbumModeEnum
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_nav.*

class NavActivity : AppCompatActivity() {

    val topLevelNavDestinations : Set<Int> = setOf(R.id.loginFragment, R.id.selectFragment)

    /*
    Deselect all items in a bottomNavigationView
     */
    private fun BottomNavigationView.uncheckAllItems() {
        menu.setGroupCheckable(0, true, false)
        for (i in 0 until menu.size()) {
            menu.getItem(i).isChecked = false
        }
        menu.setGroupCheckable(0, true, true)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val navHostFrag = getNavHostFragment()
        val frag = navHostFrag.childFragmentManager.fragments[0]
        frag.onActivityResult(requestCode,resultCode, data)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nav)

        //Set up top toolbar
        val appBarConfig = AppBarConfiguration(topLevelNavDestinations)
        val navHostFragment = getNavHostFragment()
        nav_toolbar.setupWithNavController(navHostFragment.navController,appBarConfig)

        setupBottomBarNavigation()

        //Top toolbar menu
        nav_toolbar.setOnMenuItemClickListener {
            when(it.itemId){
                R.id.accountMenuItem -> {
                    getNavHostFragment().navController.navigateUp()
                    true
                }
                else -> false
            }
        }

        //Destination Changed Listener setup
        navHostFragment.navController.addOnDestinationChangedListener { controller, destination, arguments ->
            /*
            Hide and show the top toolbar
            Inflate and clear the toolbar menu
            */
            when (destination.label) {
                resources.getResourceEntryName(R.layout.fragment_select) -> {
                    nav_bottom_bar.visibility = View.VISIBLE
                    nav_toolbar.visibility = View.VISIBLE
                    nav_toolbar.inflateMenu(R.menu.select_menu)
                    nav_bottom_bar.uncheckAllItems()
                }
                resources.getResourceEntryName(R.layout.fragment_settings) -> {
                    nav_toolbar.visibility = View.VISIBLE
                    nav_bottom_bar.visibility = View.VISIBLE
                    nav_toolbar.menu.clear()
                }
                resources.getResourceEntryName(R.layout.fragment_single_album) -> {
                    nav_toolbar.visibility = View.VISIBLE
                    nav_bottom_bar.visibility = View.VISIBLE
                    nav_toolbar.menu.clear()
                }
                else -> {
                    nav_bottom_bar.visibility = View.INVISIBLE
                    nav_toolbar.visibility = View.INVISIBLE
                    nav_toolbar.menu.clear()
                }
            }
        }
    }


    private fun getNavHostFragment(): NavHostFragment{
        return supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
    }

    private fun setupBottomBarNavigation(){
        nav_bottom_bar.setOnNavigationItemSelectedListener { item: MenuItem ->
            val navHostFragment = getNavHostFragment()
            if (!topLevelNavDestinations.contains(navHostFragment.navController.currentDestination?.id)){
                navHostFragment.navController.navigateUp()
            }
            when(item.itemId){
                R.id.historyMenuItem -> {
                    val nav = SelectFragmentDirections.actionSelectFragmentToHistory(null, SingleAlbumModeEnum.HISTORY)
                    navHostFragment.navController.navigate(nav)
                    true
                }
                R.id.settingsMenuItem -> {
                    val nav = SelectFragmentDirections.actionSelectFragmentToSettingsFragment()
                    navHostFragment.navController.navigate(nav)
                    true
                }
                R.id.photoListsMenuItem -> {
                    false
                }
                else -> {
                    false
                }
            }
        }
    }
}