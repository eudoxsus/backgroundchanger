package com.eudox.background.backgroundchanger.async.deprecated;

import java.io.File;
import java.util.concurrent.Executor;

public class BitmapTaskHandlerImpl implements BitmapTaskHandler {

    private File imgFile;
    private BitmapManager manager;

    public BitmapTaskHandlerImpl(BitmapManager manager){
        this.manager = manager;
    }

    @Override
    public File getImgFile() {
        return imgFile;
    }

    @Override
    public void setImgFile(File imgFile) {
        this.imgFile = imgFile;
    }

    @Override
    public void submitTask(Executor executor, Runnable bmRunnable) {
        if (!manager.isInProgress()){
            handleState(BitmapTaskHandler.RUNNABLE_START);
            executor.execute(bmRunnable);
        }
        else{
            handleState(BitmapTaskHandler.INPROGRESS_SUBMISSION);
        }
    }

    @Override
    public void handleState(int status) {
        int outState = -1;
        // Converts the decode state to the overall state.
        switch(status) {
            case BitmapTaskHandler.RUNNABLE_START:
                outState = BitmapManager.RUNNABLE_START;
                break;
            case BitmapTaskHandler.RUNNABLE_ERR:
                outState = BitmapManager.RUNNABLE_ERR;
                break;
            case BitmapTaskHandler.DECODE_STATE_COMPLETED:
                outState = BitmapManager.DECODE_COMPLETE;
                break;
            case BitmapTaskHandler.WALLPAPER_SET_COMPLETED:
                outState = BitmapManager.WALLPAPER_SET_COMPLETE;
                break;
            case BitmapTaskHandler.INPROGRESS_SUBMISSION:
                outState = BitmapManager.INPROGRESS_SUBMISSION;
                break;
        }
        // Calls the generalized state method
        manager.handleState(this,outState);
    }
}
