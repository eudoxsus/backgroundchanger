package com.eudox.background.backgroundchanger.facebook.random;

import android.os.Bundle;

import com.eudox.background.backgroundchanger.facebook.FacebookRequest;
import com.facebook.GraphRequest;

import java.util.Date;


public class RandomPictureUtils {

    public static long getOneDayEpoch(){
        return 86400L;
    }

    public static long getRandomDate(long startInclusive, long endInclusive){
        //EndExclusive has one day added
        long randomEpoch = (startInclusive * 1000L) + (long) (Math.random() * (((endInclusive + 86400L)  * 1000L) - (startInclusive * 1000L)));

        //long randomEpoch = ThreadLocalRandom.current().nextLong(startInclusive * 1000L,(endInclusive + 86400L)  * 1000L);
        Date randomDate = new Date(randomEpoch);
        //Zero out time
        randomDate.setHours(0);
        randomDate.setMinutes(0);
        randomDate.setSeconds(0);
        return randomDate.getTime()/1000L;
    }

    public static void requestAlbumBounded(String albumId, long sinceEpoch, long untilEpoch, GraphRequest.Callback callback){
        String pathStr = "/" + albumId + "/";
        String limiterStr = ".since(" + sinceEpoch + ")" + ".until(" + untilEpoch + ")";
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,photos.fields(id,name,picture,source)" + limiterStr);
        FacebookRequest.request(pathStr,parameters,callback);
    }

}
