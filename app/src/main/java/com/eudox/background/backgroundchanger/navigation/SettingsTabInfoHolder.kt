package com.eudox.background.backgroundchanger.navigation

import androidx.fragment.app.Fragment
import com.eudox.background.backgroundchanger.fragment.BackgroundWorkFragment
import com.eudox.background.backgroundchanger.fragment.MultiAlbumFragment

class SettingsTabInfoHolder {

    val tabInfos: List<TabInfo> = listOf(
        //BackgroundTabInfo
        object : TabInfo {
            override fun getTabTitle(): String {
                return "Background"
            }
            override fun createFragment(): Fragment {
                return BackgroundWorkFragment()
            }
        },
        //albumTabInfo
        object : TabInfo {
            override fun getTabTitle(): String {
                return "Albums"
            }
            override fun createFragment(): Fragment {
                return MultiAlbumFragment()
            }
        }
    )

}