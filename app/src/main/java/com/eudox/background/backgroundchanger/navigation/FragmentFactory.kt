package com.eudox.background.backgroundchanger.navigation

import androidx.fragment.app.Fragment

interface FragmentFactory {
    fun build() : Fragment
}