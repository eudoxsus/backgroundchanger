package com.eudox.background.backgroundchanger.adapter

interface AdapterAlbum {
    val id: String
    val name: String?
    val coverImage: String?
    val isEmpty: Boolean

    override fun equals(other: Any?): Boolean
}