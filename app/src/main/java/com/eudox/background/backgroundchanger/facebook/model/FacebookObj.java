package com.eudox.background.backgroundchanger.facebook.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public abstract class FacebookObj {
    protected String id;
    protected String name;

    @NonNull
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if(obj instanceof FacebookObj){
            FacebookObj otherObj = (FacebookObj) obj;
            return this.id.equals(otherObj.getId());
        }
        return false;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
