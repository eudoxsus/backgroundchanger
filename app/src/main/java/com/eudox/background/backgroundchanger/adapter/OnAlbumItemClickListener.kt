package com.eudox.background.backgroundchanger.adapter

import android.view.View

interface OnAlbumItemClickListener {
    fun onClick(view: View,album: AdapterAlbum)
}