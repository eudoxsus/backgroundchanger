package com.eudox.background.backgroundchanger.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.eudox.background.backgroundchanger.persist.AppDatabase
import com.eudox.background.backgroundchanger.persist.Picture
import com.eudox.background.backgroundchanger.persist.PictureDao
import org.imaginativeworld.whynotimagecarousel.CarouselItem

class SelectViewModel(application: Application) : AndroidViewModel(application) {

    private val pictureDao: PictureDao

    val pictureLiveDataList: LiveData<List<Picture>>
    val carouselItemLiveDataList: LiveData<List<CarouselItem>>

    init {
        val db = AppDatabase.getDatabase(application)
        pictureDao = db.pictureDao()
        pictureLiveDataList = db.pictureDao().getLivePicturesByLastUsedDate(10)
        carouselItemLiveDataList = Transformations.map(pictureLiveDataList) { pictures->
            val carouselItems = mutableListOf<CarouselItem>()
            for (picture in pictures){
                carouselItems.add(CarouselItem(picture.source))
            }
            carouselItems
        }
    }

}