package com.eudox.background.backgroundchanger.facebook.datasource;

import android.os.Bundle;

import com.eudox.background.backgroundchanger.facebook.FacebookRequest;
import com.eudox.background.backgroundchanger.facebook.datasource.factory.PageKeyedDataSourceCallbackFactory;
import com.eudox.background.backgroundchanger.facebook.model.AlbumJson;
import com.eudox.background.backgroundchanger.facebook.model.Cursors;
import com.eudox.background.backgroundchanger.facebook.model.PhotoData;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

public class SingleAlbumDataSource extends PageKeyedDataSource<String, PhotoData> {

    private String albumId;
    private String graphPath;
    private static String paramsStr = "id,name,photos.fields(id,name,picture,source)";
    private SingleAlbumDataSource.CallbackFactory callbackFactory = new SingleAlbumDataSource.CallbackFactory();

    public SingleAlbumDataSource(String albumId){
        this.albumId = albumId;
        graphPath = "/" + albumId + "/";
    }

    private static class CallbackFactory implements PageKeyedDataSourceCallbackFactory<String,PhotoData> {

        public GraphRequest.Callback initialCallback(final LoadInitialCallback<String, PhotoData> initCallback){
            return new GraphRequest.Callback() {
                @Override
                public void onCompleted(GraphResponse response) {
                    try {
                        //format JSON
                        AlbumJson album = new ObjectMapper().readValue(response.getRawResponse(), AlbumJson.class);
                        List<PhotoData> photos = album.getPhotos().getData();
                        Cursors cursors = album.getPhotos().getPaging().getCursors();
                        initCallback.onResult(photos,cursors.getBefore(),cursors.getAfter());
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                }
            };
        }

        public GraphRequest.Callback beforeCallback(final LoadCallback<String, PhotoData> befCallback){
            return new GraphRequest.Callback() {
                @Override
                public void onCompleted(GraphResponse response) {
                    try {
                        //format JSON
                        AlbumJson album = new ObjectMapper().readValue(response.getRawResponse(), AlbumJson.class);
                        if(album.getPhotos() != null){
                            List<PhotoData> photos = album.getPhotos().getData();
                            Cursors cursors = album.getPhotos().getPaging().getCursors();
                            befCallback.onResult(photos,cursors.getBefore());
                        }
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                }
            };
        }

        public GraphRequest.Callback afterCallback(final LoadCallback<String, PhotoData> aftCallback){
            return new GraphRequest.Callback() {
                @Override
                public void onCompleted(GraphResponse response) {
                    try {
                        //format JSON
                        AlbumJson album = new ObjectMapper().readValue(response.getRawResponse(), AlbumJson.class);
                        if(album.getPhotos() != null){
                            List<PhotoData> photos = album.getPhotos().getData();
                            Cursors cursors = album.getPhotos().getPaging().getCursors();
                            aftCallback.onResult(photos,cursors.getAfter());
                        }
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                }
            };
        }
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<String> params, @NonNull final LoadInitialCallback<String, PhotoData> callback) {

        Bundle parameters = new Bundle();
        parameters.putString("fields", paramsStr);

        FacebookRequest.request(graphPath,parameters,callbackFactory.initialCallback(callback));
    }

    @Override
    public void loadBefore(@NonNull LoadParams<String> params, @NonNull final LoadCallback<String, PhotoData> callback) {

        String before = ".before(" + params.key + ")";

        Bundle parameters = new Bundle();
        parameters.putString("fields", paramsStr + before);

        FacebookRequest.request(graphPath,parameters,callbackFactory.beforeCallback(callback));
    }

    @Override
    public void loadAfter(@NonNull LoadParams<String> params, @NonNull final LoadCallback<String, PhotoData> callback) {

        String after = ".after(" + params.key + ")";

        Bundle parameters = new Bundle();
        parameters.putString("fields", paramsStr + after);

        FacebookRequest.request(graphPath,parameters,callbackFactory.afterCallback(callback));
    }

    public String getAlbumId() {
        return albumId;
    }
}
