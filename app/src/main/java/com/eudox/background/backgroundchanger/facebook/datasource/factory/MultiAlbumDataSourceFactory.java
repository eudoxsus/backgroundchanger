package com.eudox.background.backgroundchanger.facebook.datasource.factory;

import com.eudox.background.backgroundchanger.adapter.AdapterAlbum;
import com.eudox.background.backgroundchanger.facebook.datasource.MultiAlbumDataSource;
import com.eudox.background.backgroundchanger.facebook.model.AlbumJson;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

public class MultiAlbumDataSourceFactory extends DataSource.Factory<String, AdapterAlbum> {

    private MutableLiveData<PageKeyedDataSource<String, AdapterAlbum>> multiAlbumLiveDataSource = new MutableLiveData<>();

    @NonNull
    @Override
    public DataSource create() {
        MultiAlbumDataSource multiAlbumDataSource = new MultiAlbumDataSource();
        multiAlbumLiveDataSource.postValue(multiAlbumDataSource);
        return multiAlbumDataSource;
    }

    public MutableLiveData<PageKeyedDataSource<String, AdapterAlbum>> getMultiAlbumLiveDataSource() {
        return multiAlbumLiveDataSource;
    }
}
