package com.eudox.background.backgroundchanger.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.eudox.background.backgroundchanger.facebook.datasource.factory.SinglePhotoDataSourceFactory
import com.eudox.background.backgroundchanger.facebook.model.PhotoData

class SingleAlbumViewModelK(application: Application, albumId: String) : AndroidViewModel(application) {

    val photoDataPagedList: LiveData<PagedList<PhotoData>>

    init {
        //liveDataSource = singlePhotoDataSourceFactory.getSingleAlbumLiveDataSource();
        val singlePhotoDataSourceFactory = SinglePhotoDataSourceFactory(albumId)
        val config = PagedList.Config.Builder().setEnablePlaceholders(false).setPageSize(20).build()
        photoDataPagedList = LivePagedListBuilder(singlePhotoDataSourceFactory, config).build()
    }

}