package com.eudox.background.backgroundchanger.facebook.model;

public class TopAlbumContainer extends FacebookObj {

    private AlbumContainer albums;

    public AlbumContainer getAlbums() {
        return albums;
    }

    public void setAlbums(AlbumContainer albums) {
        this.albums = albums;
    }
}
