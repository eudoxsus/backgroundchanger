package com.eudox.background.backgroundchanger.viewmodel.enums

enum class SingleAlbumModeEnum {
    ALBUM,
    HISTORY
}