package com.eudox.background.backgroundchanger.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.eudox.background.backgroundchanger.persist.AppDatabase
import com.eudox.background.backgroundchanger.persist.Picture
import com.eudox.background.backgroundchanger.persist.PictureDao
import kotlinx.coroutines.async

class HistoryViewModel(application: Application) : AndroidViewModel(application) {

    private val pictureDao: PictureDao = AppDatabase.getDatabase(application.applicationContext).pictureDao()

    suspend fun getPicturePagedList(): LiveData<PagedList<Picture>> {
        val config = PagedList.Config.Builder().setEnablePlaceholders(false).setPageSize(20).build()
        return viewModelScope.async {
            LivePagedListBuilder(pictureDao.getPagedPicturesByLastUsedDate(),config).build()
        }.await()
    }

}