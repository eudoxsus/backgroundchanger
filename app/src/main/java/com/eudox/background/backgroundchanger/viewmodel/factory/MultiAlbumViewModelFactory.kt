package com.eudox.background.backgroundchanger.viewmodel.factory

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.eudox.background.backgroundchanger.viewmodel.MultiAlbumViewModel
import com.eudox.background.backgroundchanger.viewmodel.enums.MultiAlbumModeEnum

class MultiAlbumViewModelFactory(private val application: Application,private val modeEnum: MultiAlbumModeEnum) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        when (modeEnum) {
            MultiAlbumModeEnum.FACEBOOK -> return MultiAlbumViewModel(application) as T
            else -> {
                return MultiAlbumViewModel(application) as T
            }
        }
    }
}