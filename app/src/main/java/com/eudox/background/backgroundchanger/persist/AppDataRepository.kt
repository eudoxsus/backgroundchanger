package com.eudox.background.backgroundchanger.persist

import android.content.Context
import androidx.lifecycle.LiveData
import com.eudox.background.backgroundchanger.async.WallpaperSetter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import java.lang.IllegalArgumentException
import java.util.*
import java.util.concurrent.TimeUnit

class AppDataRepository(appDatabase: AppDatabase) {

    private val appDataDao: AppDataDao = appDatabase.appDataDao()

    enum class AppDataSubmitEnum{
        RAND_END_DTE,
        RAND_START_DTE,
        WP_VALUE,
        WP_TU,
        WP_SETTER,
        WP_NEXT_WORK_DTE
    }

    enum class AppDataSubmitResult{
        SAME_VALUE,
        INVALID_NEW_VALUE,
        UPDATED,
        INSERTED,
    }

    companion object{
        @Volatile
        private var instance: AppDataRepository? = null

        fun getInstance(context: Context): AppDataRepository? {
            if (instance == null) {
                synchronized(AppDataRepository::class.java) {
                    if (instance == null) {
                        instance = AppDataRepository(AppDatabase.getDatabase(context))
                    }
                }
            }
            return instance
        }
    }

    suspend fun getAppDataById(appId: String,param: String): AppData?{
        return appDataDao.getAppDataById(appId,param)
    }

    fun getRandDatesLiveData(): LiveData<List<AppData>>{
        return appDataDao.getRandDatesLive()
    }

    fun getWorkPeriodValuesLive(): LiveData<List<AppData>> {
        return appDataDao.getAppDataByAppLive(AppData.APP_ID_WORK)
    }

    suspend fun delete(appId: String, paramId: String){
        appDataDao.deleteAppDataById(appId, paramId)
    }

    private suspend fun handleGeneric(appId: String, paramId: String, validateNewAppDataValue: (String) -> Boolean, newValue: String): AppDataSubmitResult{
        //Validate the new appData value
        if (validateNewAppDataValue.invoke(newValue)){
            val currentAppData = appDataDao.getAppDataById(appId, paramId)
            val newAppData = AppData(appId,paramId,newValue)
            //Insert if the current is null, update if nonnull
            return if (currentAppData != null){
                if (currentAppData != newAppData){
                    appDataDao.update(newAppData)
                    AppDataSubmitResult.UPDATED
                } else{
                    AppDataSubmitResult.SAME_VALUE
                }
            } else{
                appDataDao.insert(newAppData)
                AppDataSubmitResult.INSERTED
            }
        }
        else{
            return AppDataSubmitResult.INVALID_NEW_VALUE
        }
    }

    suspend fun handleSubmit(newValue: String, appDataSubmit: AppDataSubmitEnum) : AppDataSubmitResult {
        return CoroutineScope(Dispatchers.IO).async {
            when(appDataSubmit){
                AppDataSubmitEnum.RAND_START_DTE ->{
                    handleGeneric(AppData.APP_ID_RAND, AppData.PAR_RAND_START_DTE,{ value -> value.toLongOrNull() != null},newValue)
                    /*
                    val currentStartDate = appDataDao.getAppDataById(AppData.APP_ID_RAND, AppData.PAR_RAND_START_DTE)
                    if (newValue.toLongOrNull() != null){
                        val newStartDate = AppData(AppData.APP_ID_RAND, AppData.PAR_RAND_START_DTE, newValue)
                        if (currentStartDate != null){
                            if (currentStartDate.value != newStartDate.value){
                                appDataDao.update(newStartDate)
                                return@async "Start date updated"
                            }
                            else{
                                return@async "No change"
                            }
                        }
                        else{
                            appDataDao.insert(newStartDate)
                            return@async "Start date inserted"
                        }
                    }
                    else{
                        return@async "Invalid format"
                    }
                     */
                }
                AppDataSubmitEnum.RAND_END_DTE -> {
                    handleGeneric(AppData.APP_ID_RAND, AppData.PAR_RAND_END_DTE,{ value -> value.toLongOrNull() != null},newValue)
                    /*
                    val currentEndDate = appDataDao.getAppDataById(AppData.APP_ID_RAND, AppData.PAR_RAND_END_DTE)
                    if (newValue.toLongOrNull() != null){
                        val newEndDate = AppData(AppData.APP_ID_RAND, AppData.PAR_RAND_END_DTE, newValue)
                        if (currentEndDate != null){
                            if (currentEndDate.value != newEndDate.value){
                                appDataDao.update(newEndDate)
                                return@async "End date updated"
                            }
                            else{
                                return@async "No change"
                            }
                        }
                        else{
                            appDataDao.insert(newEndDate)
                            return@async "End date inserted"
                        }
                    }
                    else{
                        return@async "Invalid format"
                    }

                     */
                }
                AppDataSubmitEnum.WP_VALUE -> {
                    handleGeneric(AppData.APP_ID_WORK, AppData.PAR_WP_TIME_VAL,{ value -> value.toIntOrNull() != null},newValue)
                    /*
                    val currentWpVal = appDataDao.getAppDataById(AppData.APP_ID_WORK, AppData.PAR_WP_TIME_VAL)
                    val newVal = newValue.toIntOrNull()
                    if (newVal != null){
                        val newValueApp = AppData(AppData.APP_ID_WORK, AppData.PAR_WP_TIME_VAL, newValue)
                        if (currentWpVal != null){
                            if (currentWpVal.value != newValueApp.value){
                                appDataDao.update(newValueApp)
                                return@async "Period value updated"
                            }
                            else{
                                return@async "No change"
                            }
                        }
                        else{
                            appDataDao.insert(newValueApp)
                            return@async "Period value inserted"
                        }
                    }
                    else{
                        return@async "Invalid format"
                    }

                     */
                }
                AppDataSubmitEnum.WP_TU -> {
                    handleGeneric(AppData.APP_ID_WORK, AppData.PAR_WP_TU,{ value ->
                        try {
                            TimeUnit.valueOf(value.toUpperCase(Locale.ROOT))
                            true
                        }
                        catch (e: IllegalArgumentException){
                            false
                        }
                    },newValue)

                    /*
                    val currentTimeUnit = appDataDao.getAppDataById(AppData.APP_ID_WORK, AppData.PAR_WP_TU)
                    try {
                        val newVal = TimeUnit.valueOf(newValue.toUpperCase(Locale.ROOT))
                        val newTimeUnit = AppData(AppData.APP_ID_WORK, AppData.PAR_WP_TU, newValue)
                        if (currentTimeUnit != null){
                            if (currentTimeUnit.value != newTimeUnit.value){
                                appDataDao.update(newTimeUnit)
                                return@async "Time Unit value updated"
                            }
                            else{
                                return@async "No change"
                            }
                        }
                        else{
                            appDataDao.insert(newTimeUnit)
                            return@async "Time Unit value inserted"
                        }

                    }
                    catch (e: IllegalArgumentException){
                        return@async "Invalid string to enum conversion"
                    }

                     */
                }
                AppDataSubmitEnum.WP_SETTER -> {
                    handleGeneric(AppData.APP_ID_WORK, AppData.PAR_WP_SETTER,{ value ->
                        try {
                            WallpaperSetter.SetterFlag.valueOf(value.toUpperCase(Locale.ROOT))
                            true
                        }
                        catch (e: IllegalArgumentException){
                            false
                        }
                    },newValue)
                    /*
                    val currentSetter = appDataDao.getAppDataById(AppData.APP_ID_WORK, AppData.PAR_WP_SETTER)
                    try {
                        val newVal = WallpaperSetter.SetterFlag.valueOf(newValue.toUpperCase(Locale.ROOT))
                        val newSetter = AppData(AppData.APP_ID_WORK, AppData.PAR_WP_SETTER, newValue)
                        if (currentSetter != null){
                            if (currentSetter.value != newSetter.value){
                                appDataDao.update(newSetter)
                                return@async "Setter Flag value updated"
                            }
                            else{
                                return@async "No change"
                            }
                        }
                        else{
                            appDataDao.insert(newSetter)
                            return@async "Setter Flag value inserted"
                        }

                    }
                    catch (e: IllegalArgumentException){
                        return@async "Invalid string to enum conversion"
                    }
                     */
                }
                AppDataSubmitEnum.WP_NEXT_WORK_DTE -> {
                    handleGeneric(AppData.APP_ID_WORK, AppData.PAR_WP_NEXT_WORK_DTE,{ value ->
                        value.toLongOrNull() != null}
                            ,newValue)
                }
            }
        }.await()
    }



}