package com.eudox.background.backgroundchanger.facebook;

import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;

public class FacebookRequest {

    public static void request(String graphPath, Bundle reqBundle, GraphRequest.Callback callback){
        GraphRequest request = GraphRequest.newGraphPathRequest(
                AccessToken.getCurrentAccessToken(),
                graphPath,
                callback
        );

        request.setParameters(reqBundle);
        request.executeAsync();
    }
}
