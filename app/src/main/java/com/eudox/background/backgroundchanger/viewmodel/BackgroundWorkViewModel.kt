package com.eudox.background.backgroundchanger.viewmodel

import android.app.Application
import android.content.Context
import android.content.DialogInterface
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.NumberPicker
import android.widget.Spinner
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.WorkManager
import com.eudox.background.backgroundchanger.R
import com.eudox.background.backgroundchanger.background.WallpaperWorker
import com.eudox.background.backgroundchanger.fragment.dialog.TimePeriodDialogFragment
import com.eudox.background.backgroundchanger.persist.AppData
import com.eudox.background.backgroundchanger.persist.AppDataRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class BackgroundWorkViewModel(application: Application) : AndroidViewModel(application), TimePeriodDialogFragment.InitTimeViewProvider, TimePeriodDialogFragment.TimePeriodDialogListenerProvider{

    private val appDataRepo : AppDataRepository = AppDataRepository.getInstance(application)!!

    val randDatesLive: LiveData<List<AppData>> = appDataRepo.getRandDatesLiveData()
    val timeDataLive: LiveData<List<AppData>> = appDataRepo.getWorkPeriodValuesLive()

    private var _startDateLong: Long = 0
    private var _endDateLong: Long = 0
    private var _nextWorkDateLong: Long = 0

    private var _timeUnit: TimeUnit = TimeUnit.NANOSECONDS
    private var _periodValue: Int = 0

    val startDateLong: Long get(){
        return _startDateLong
    }

    val endDateLong: Long get() {
        return _endDateLong
    }

    val nextWorkDateLong: Long get() {
        return _nextWorkDateLong
    }

    val timeUnit: TimeUnit get(){
        return _timeUnit
    }

    val periodValue: Int get() {
        return _periodValue
    }

    suspend fun observeRandDatesLive(randDates: List<AppData>) : Pair<String, String>{
        randDates.forEach {
            if (it.paramId == AppData.PAR_RAND_START_DTE){
                _startDateLong = it.value.toLong()
            }
            else if (it.paramId == AppData.PAR_RAND_END_DTE){
                _endDateLong = it.value.toLong()
            }
        }
        return if (_startDateLong != 0L && _endDateLong != 0L){
            val startDte = viewModelScope.async(Dispatchers.Default) {
                val dateFormat = SimpleDateFormat.getDateInstance()
                val str = dateFormat.format(Date(_startDateLong * 1000))
                str
            }
            val endDte = viewModelScope.async(Dispatchers.Default) {
                val dateFormat = SimpleDateFormat.getDateInstance()
                val str = dateFormat.format(Date(_endDateLong * 1000))
                str
            }
            Pair<String, String>(startDte.await(), endDte.await())
        }
        else{
            Pair("", "")
        }
    }

    /**
     * Synchronous version of getDisplayDatePair. Do not use for production, just for performance measuring
     *
    fun getDisplayDatePairSync() : Pair<String,String>{
        return if (startDateLong != 0L && endDateLong != 0L){
            val dateFormat = SimpleDateFormat.getDateInstance()
            Pair<String,String>(dateFormat.format(Date(startDateLong*1000)),dateFormat.format(Date(endDateLong*1000)))
        }
        else{
            Pair("","")
        }
    }
    */

    fun formatDatePickerValToStr(year: Int, month: Int, dayOfMonth: Int): String{
        val dateFormat = SimpleDateFormat.getDateInstance()
        val gregCalendar = GregorianCalendar(year, month, dayOfMonth)
        return dateFormat.format(gregCalendar.time)
    }

    private fun datePickerValToLong(year: Int, month: Int, dayOfMonth: Int): Long{
        val gregCalendar = GregorianCalendar(year, month, dayOfMonth)
        return gregCalendar.time.time/1000
    }

    suspend fun submitDatePickerDialogResult(year: Int, month: Int, dayOfMonth: Int, appDataVal: AppDataRepository.AppDataSubmitEnum) : String{
        return appDataRepo.handleSubmit(datePickerValToLong(year, month, dayOfMonth).toString(), appDataVal).name
    }

    fun canEnableWork(): Pair<Boolean,String>{
        var enable = true
        val stringBuilder = StringBuilder()
        if (periodValue == 0 || _timeUnit == TimeUnit.NANOSECONDS){
            enable= false
            stringBuilder.append("A valid time period for work was not chosen.\n")
        }
        if (_startDateLong == 0L){
            enable= false
            stringBuilder.append("A valid start date for choosing pictures.\n")
        }
        if (_endDateLong == 0L){
            enable= false
            stringBuilder.append("A valid end date for choosing pictures.\n")
        }

        if (!enable){
            stringBuilder.insert(0,"Cannot enable because of the following:\n")
        }
        return Pair(enable,stringBuilder.toString())
    }

    fun enqueueWorkPeriodic(context: Context){
        val randomPicWork = WallpaperWorker.build(periodValue.toLong(), timeUnit)
        WorkManager.getInstance(context).enqueueUniquePeriodicWork(WallpaperWorker.RANDOM_WORK_NAME, ExistingPeriodicWorkPolicy.REPLACE,randomPicWork)
        CoroutineScope(Dispatchers.IO).launch {
            val dateObj = Date()
            appDataRepo.handleSubmit((dateObj.time + TimeUnit.MILLISECONDS.convert(_periodValue.toLong(),timeUnit)).toString(),AppDataRepository.AppDataSubmitEnum.WP_NEXT_WORK_DTE)
        }
    }

    private fun enqueueWorkPeriodic(periodVal: Int,timeUnit: TimeUnit){
        val randomPicWork = WallpaperWorker.build(periodVal.toLong(), timeUnit)
        WorkManager.getInstance(getApplication()).enqueueUniquePeriodicWork(WallpaperWorker.RANDOM_WORK_NAME, ExistingPeriodicWorkPolicy.REPLACE,randomPicWork)
        //Need to update the value in
        CoroutineScope(Dispatchers.IO).launch {
            val dateObj = Date()
            appDataRepo.handleSubmit((dateObj.time + TimeUnit.MILLISECONDS.convert(periodVal.toLong(),timeUnit)).toString(),AppDataRepository.AppDataSubmitEnum.WP_NEXT_WORK_DTE)
        }
    }

    fun dequeueWorkPeriodic(context: Context){
        WorkManager.getInstance(context).cancelUniqueWork(WallpaperWorker.RANDOM_WORK_NAME)
    }

    private fun setPickerBounds(timeUnit: TimeUnit, picker: NumberPicker){
        val min: Int
        val max: Int

        when(timeUnit){
            TimeUnit.MINUTES -> {
                min = 15
                max = 59
            }
            TimeUnit.HOURS -> {
                min = 1
                max = 23
            }
            TimeUnit.DAYS -> {
                min = 1
                max = 7
            }
            else -> {
                min = 1
                max = 1
            }
        }

        picker.wrapSelectorWheel = false

        picker.minValue = min
        picker.maxValue = max
        if (periodValue != 0){
            picker.value = periodValue
        }

        if (periodValue < min || periodValue > max){
            picker.value = picker.minValue
        }
    }

    fun observeTimePeriodDataLive(timeUnitData: List<AppData>){
        timeUnitData.forEach {
            when(it.paramId){
                AppData.PAR_WP_TU -> _timeUnit = TimeUnit.valueOf(it.value.toUpperCase(Locale.ROOT))
                AppData.PAR_WP_TIME_VAL-> _periodValue = it.value.toInt()
                AppData.PAR_WP_NEXT_WORK_DTE -> _nextWorkDateLong = it.value.toLong()
            }
        }
    }

    override fun getTimeDialogInit(): (View) -> Unit {

        return { timeView: View ->
            val picker = timeView.findViewById<NumberPicker>(R.id.durationNumberPicker)
            val spinner = timeView.findViewById<Spinner>(R.id.timeUnitSpinner)

            //Set the spinner choices
            ArrayAdapter.createFromResource(getApplication(), R.array.timeUnit_array, android.R.layout.simple_spinner_item).also { adapter -> adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinner.adapter = adapter
            }

            //Set the initial selection
            if (timeUnit != TimeUnit.NANOSECONDS){
                when(timeUnit){
                    TimeUnit.MINUTES -> {
                        spinner.setSelection(0)
                    }
                    TimeUnit.HOURS -> {
                        spinner.setSelection(1)
                    }
                    TimeUnit.DAYS -> {
                        spinner.setSelection(2)
                    }
                }
            }
            else{
                _timeUnit = TimeUnit.HOURS
                spinner.setSelection(1)
            }

            //Spinner OnItemSelected
            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    val selectedItem = parent?.getItemAtPosition(position) as String
                    setPickerBounds(TimeUnit.valueOf(selectedItem.toUpperCase(Locale.ROOT)), picker)
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
            }

        }
    }

    override fun getOnPositiveButtonClickListener(): (dialog: DialogInterface, timeView: View) -> Unit {
        return { dialog: DialogInterface, timeView: View ->
            CoroutineScope(Dispatchers.IO).launch {
                val picker = timeView.findViewById<NumberPicker>(R.id.durationNumberPicker)
                val spinner = timeView.findViewById<Spinner>(R.id.timeUnitSpinner)

                appDataRepo.handleSubmit(picker.value.toString(), AppDataRepository.AppDataSubmitEnum.WP_VALUE)
                appDataRepo.handleSubmit(spinner.selectedItem.toString(), AppDataRepository.AppDataSubmitEnum.WP_TU)

                enqueueWorkPeriodic(picker.value,TimeUnit.valueOf(spinner.selectedItem.toString().toUpperCase(Locale.ROOT)))
            }
        }
    }

    override fun getOnNegativeButtonClickListener(): (dialog: DialogInterface, timeView: View) -> Unit {
        return { dialog, timeView ->
            dialog.cancel()
        }
    }
}