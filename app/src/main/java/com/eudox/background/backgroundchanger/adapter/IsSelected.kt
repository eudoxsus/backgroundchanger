package com.eudox.background.backgroundchanger.adapter

interface IsSelected {
    suspend fun isSelected(album: AdapterAlbum) : Boolean
}