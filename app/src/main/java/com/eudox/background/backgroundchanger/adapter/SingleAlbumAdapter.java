package com.eudox.background.backgroundchanger.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.eudox.background.backgroundchanger.R;
import com.eudox.background.backgroundchanger.facebook.model.PhotoData;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;

import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

public class SingleAlbumAdapter extends PagedListAdapter<PhotoData, SingleAlbumAdapter.ViewHolder> {

    private Context context;
    private View.OnClickListener onClickListener;

    public SingleAlbumAdapter(Context context,View.OnClickListener onClickListener) {
        super(DIFF_CALLBACK);
        this.context = context;
        this.onClickListener = onClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.cardImage);
        }

        public void bindTo(PhotoData photoData){
            Glide.with(context)
                    .load(photoData.getSource())
                    .into(imageView);

            itemView.setOnClickListener(onClickListener);
            /*
            File cacheDir = context.getCacheDir();
            BitmapTaskHandler taskHandler = new BitmapTaskHandlerImpl(context);
            Runnable runnable = new BitmapDecodeRunnable(taskHandler,cacheDir,photoData.getSource());
            taskHandler.submitTask(executor,runnable);
            */
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.img_list_cell,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PhotoData photoData = getItem(position);
        if (photoData != null){
            holder.bindTo(photoData);
        }
        else{
            Toast.makeText(context,"Item is null",Toast.LENGTH_LONG).show();
        }
    }

    private static DiffUtil.ItemCallback<PhotoData> DIFF_CALLBACK = new DiffUtil.ItemCallback<PhotoData>() {
        @Override
        public boolean areItemsTheSame(@NonNull PhotoData oldItem, @NonNull PhotoData newItem) {
            return oldItem.getId().equals(newItem.getId());
        }

        @Override
        public boolean areContentsTheSame(@NonNull PhotoData oldItem, @NonNull PhotoData newItem) {
            return oldItem.equals(newItem);
        }
    };
}
