package com.eudox.background.backgroundchanger.persist

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface AppDataDao {

    @Query("SELECT * FROM " + AppData.TABLE_NAME + " WHERE appId = :appId AND paramId = :paramId")
    suspend fun getAppDataById(appId : String,paramId : String): AppData?

    @Query("SELECT * FROM " + AppData.TABLE_NAME + " WHERE appId = :appId")
    suspend fun getAppDataByApp(appId : String): List<AppData>

    @Query("SELECT * FROM " + AppData.TABLE_NAME + " WHERE appId = :appId")
    fun getAppDataByAppLive(appId : String): LiveData<List<AppData>>

    @Query("DELETE FROM " + AppData.TABLE_NAME + " WHERE appId = :appId AND paramId = :paramId")
    suspend fun deleteAppDataById(appId : String,paramId : String): Int

    @Query("SELECT * FROM " + AppData.TABLE_NAME + " WHERE appId = \"" + AppData.APP_ID_RAND + "\" AND (paramId = \"" + AppData.PAR_RAND_START_DTE + "\" OR " + "paramId = \"" + AppData.PAR_RAND_END_DTE + "\")")
    fun getRandDatesLive(): LiveData<List<AppData>>

    @Query("SELECT * FROM " + AppData.TABLE_NAME + " WHERE appId = \"" + AppData.APP_ID_RAND + "\" AND (paramId = \"" + AppData.PAR_RAND_START_DTE + "\" OR " + "paramId = \"" + AppData.PAR_RAND_END_DTE + "\")")
    suspend fun getRandDates(): List<AppData>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(appData: AppData)

    @Update
    suspend fun update(appData: AppData)
}