package com.eudox.background.backgroundchanger.facebook.model;

public class Cursors {

    protected String before;
    protected String after;

    public void setAfter(String after) {
        this.after = after;
    }

    public void setBefore(String before) {
        this.before = before;
    }

    public String getBefore(){
        return before;
    }

    public String getAfter() {
        return after;
    }
}
