package com.eudox.background.backgroundchanger.async.deprecated;

import java.io.File;
import java.util.concurrent.Executor;

public interface BitmapTaskHandler {

    int RUNNABLE_START = 111111;

    int RUNNABLE_ERR = -111111;

    int DECODE_STATE_COMPLETED = 134186;

    int WALLPAPER_SET_COMPLETED = 148741;



    int INPROGRESS_SUBMISSION = 876955;

    String IMG_EXT = ".PNG";

    File getImgFile();

    void setImgFile(File imgFile);

    void submitTask(Executor executor, Runnable bmRunnable);

    void handleState(int status);
}
