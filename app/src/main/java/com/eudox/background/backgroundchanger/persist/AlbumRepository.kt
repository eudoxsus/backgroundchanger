package com.eudox.background.backgroundchanger.persist

import androidx.lifecycle.LiveData

class AlbumRepository(private val albumDao: AlbumDao) {

    val allAlbums: LiveData<List<Album>> = albumDao.getAllAlbumsLive()

    suspend fun insert(album: Album){
        albumDao.insert(album)
    }

}