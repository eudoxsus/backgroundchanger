package com.eudox.background.backgroundchanger.facebook.datasource;

import android.os.Bundle;

import com.eudox.background.backgroundchanger.adapter.AdapterAlbum;
import com.eudox.background.backgroundchanger.facebook.FacebookRequest;
import com.eudox.background.backgroundchanger.facebook.datasource.factory.PageKeyedDataSourceCallbackFactory;
import com.eudox.background.backgroundchanger.facebook.model.Cursors;
import com.eudox.background.backgroundchanger.facebook.model.TopAlbumContainer;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

public class MultiAlbumDataSource extends PageKeyedDataSource<String, AdapterAlbum> {

    private static String mePath = "/me/";
    private static String paramsStr = "albums.fields(id,name,photos.fields(id,name,source).limit(1))";
    private CallbackFactory callbackFactory = new CallbackFactory();

    @Override
    public void loadInitial(@NonNull LoadInitialParams<String> params, @NonNull LoadInitialCallback<String, AdapterAlbum> callback) {
        Bundle parameters = new Bundle();
        parameters.putString("fields", paramsStr);

        FacebookRequest.request(mePath,parameters,callbackFactory.initialCallback(callback));
    }

    @Override
    public void loadBefore(@NonNull LoadParams<String> params, @NonNull LoadCallback<String, AdapterAlbum> callback) {
        String before = ".before(" + params.key + ")";

        Bundle parameters = new Bundle();
        parameters.putString("fields", paramsStr + before);

        FacebookRequest.request(mePath,parameters,callbackFactory.beforeCallback(callback));
    }

    @Override
    public void loadAfter(@NonNull LoadParams<String> params, @NonNull LoadCallback<String, AdapterAlbum> callback) {
        String after = ".after(" + params.key + ")";

        Bundle parameters = new Bundle();
        parameters.putString("fields", paramsStr + after);

        FacebookRequest.request(mePath,parameters,callbackFactory.afterCallback(callback));
    }

    private static class CallbackFactory implements PageKeyedDataSourceCallbackFactory<String, AdapterAlbum> {

        public GraphRequest.Callback initialCallback(final LoadInitialCallback<String, AdapterAlbum> initCallback){
            return new GraphRequest.Callback() {
                @Override
                public void onCompleted(GraphResponse response) {
                    try {
                        //format JSON
                        TopAlbumContainer albums = new ObjectMapper().readValue(response.getRawResponse(), TopAlbumContainer.class);
                        List<? extends AdapterAlbum> albumList = albums.getAlbums().getData();
                        Cursors cursors = albums.getAlbums().getPaging().getCursors();
                        initCallback.onResult((List<AdapterAlbum>) albumList,cursors.getBefore(),cursors.getAfter());
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                }
            };
        }

        public GraphRequest.Callback beforeCallback(final LoadCallback<String, AdapterAlbum> befCallback){
            return new GraphRequest.Callback() {
                @Override
                public void onCompleted(GraphResponse response) {
                    try {
                        //format JSON
                        TopAlbumContainer albums = new ObjectMapper().readValue(response.getRawResponse(), TopAlbumContainer.class);

                        if (albums.getAlbums() != null){
                            List<? extends AdapterAlbum> albumList = albums.getAlbums().getData();
                            Cursors cursors = albums.getAlbums().getPaging().getCursors();
                            String beforeKey = cursors.getBefore();
                            befCallback.onResult((List<AdapterAlbum>) albumList,beforeKey);
                        }

                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                }
            };
        }

        public GraphRequest.Callback afterCallback(final LoadCallback<String, AdapterAlbum> aftCallback){
            return new GraphRequest.Callback() {
                @Override
                public void onCompleted(GraphResponse response) {
                    try {
                        //format JSON
                        TopAlbumContainer albums = new ObjectMapper().readValue(response.getRawResponse(), TopAlbumContainer.class);

                        if (albums.getAlbums() != null){
                            List<? extends AdapterAlbum> albumList = albums.getAlbums().getData();
                            Cursors cursors = albums.getAlbums().getPaging().getCursors();
                            String afterKey = cursors.getAfter();
                            aftCallback.onResult((List<AdapterAlbum>) albumList,afterKey);
                        }
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                }
            };
        }
    }
}
