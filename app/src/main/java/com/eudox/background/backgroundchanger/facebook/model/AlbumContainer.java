package com.eudox.background.backgroundchanger.facebook.model;

import java.util.List;

public class AlbumContainer extends FacebookObj {
    private List<AlbumJson> data;
    private Paging paging;

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    public List<AlbumJson> getData() {
        return data;
    }

    public void setData(List<AlbumJson> data) {
        this.data = data;
    }
}
