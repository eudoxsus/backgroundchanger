package com.eudox.background.backgroundchanger.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.Toast;

import com.eudox.background.backgroundchanger.R;
import com.eudox.background.backgroundchanger.async.deprecated.BitmapManager;
import com.eudox.background.backgroundchanger.async.deprecated.BitmapTaskHandler;
import com.eudox.background.backgroundchanger.async.deprecated.BitmapTaskHandlerImpl;
import com.eudox.background.backgroundchanger.async.deprecated.WallpaperSettingRunnable;

import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SingleAlbumActivity extends AppCompatActivity implements BitmapManager {

    private RecyclerView gridRecycler;
    private ExecutorService executorService;
    private Handler handler;
    private AppCompatActivity context;
    private WallpaperManager wallpaperManager;

    private volatile boolean inProgress = false;

    private Toast currentToast;

    private static int DP_WID = 100;

    @Override
    protected void onDestroy() {
        executorService.shutdown();
        super.onDestroy();
    }

    public static int calculateNoOfColumns(Context context, float columnWidthDp) { // For example columnWidthdp=180
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float screenWidthDp = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (screenWidthDp / columnWidthDp + 0.5); // +0.5 for correct rounding to int.
        return noOfColumns;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_single_album);
        this.context = this;
        gridRecycler = findViewById(R.id.gridRecycler);
        gridRecycler.setLayoutManager(new GridLayoutManager(context,calculateNoOfColumns(context,DP_WID)));

        wallpaperManager = WallpaperManager.getInstance(context);

        /*
        Intent intent = getIntent();
        String albumId = intent.getStringExtra(MultiAlbumFragment.ALBUM_ID_KEY);
         */

        executorService = Executors.newSingleThreadExecutor();
        handler = new Handler(Looper.getMainLooper()){
            @Override
            public void handleMessage(@NonNull Message msg) {
                switch (msg.what) {
                    // The decoding is done
                    case DECODE_COMPLETE:
                        /*
                         * Passes image to intent
                         */
                        WindowManager windowManager = context.getWindowManager();
                        DisplayMetrics metrics = new DisplayMetrics();
                        windowManager.getDefaultDisplay().getMetrics(metrics);
                        int height = metrics.heightPixels;
                        int width = metrics.widthPixels;
                        BitmapTaskHandler bmHandler = (BitmapTaskHandler) msg.obj;
                        CropImage.activity(Uri.fromFile(bmHandler.getImgFile())).
                                setAutoZoomEnabled(true).
                                setInitialCropWindowPaddingRatio(0).
                                setFixAspectRatio(true).
                                setAspectRatio(width,height).
                                start(context);
                        break;
                    case WALLPAPER_SET_COMPLETE:
                    case RUNNABLE_ERR:
                        inProgress = false;
                        break;
                    case RUNNABLE_START:
                        inProgress = true;
                        break;
                    case INPROGRESS_SUBMISSION:
                        if (currentToast != null) {
                            currentToast.cancel();
                        }
                        currentToast = Toast.makeText(SingleAlbumActivity.this, "Please wait for current image task to finish", Toast.LENGTH_SHORT);
                        currentToast.show();
                        break;
                    default:
                        /*
                         * Pass along other messages from the UI
                         */
                        super.handleMessage(msg);
                }
            }
        };

        /*
        SingleAlbumViewModel singleAlbumViewModel = new ViewModelProvider(this,new PictureCollectionViewModelFactory("albumId")).get(SingleAlbumViewModel.class);

        final SingleAlbumAdapter adapter = new SingleAlbumAdapter(this);

        singleAlbumViewModel.photoDataPagedList.observe(this, new Observer<PagedList<PhotoData>>() {
            @Override
            public void onChanged(PagedList<PhotoData> photos) {
                adapter.submitList(photos);
            }
        });



        gridRecycler.setAdapter(adapter);
         */
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                BitmapTaskHandler bitmapTaskHandler = new BitmapTaskHandlerImpl((SingleAlbumActivity)context);
                bitmapTaskHandler.setImgFile(new File(resultUri.getPath()));
                WallpaperSettingRunnable wallpaperSettingRunnable = new WallpaperSettingRunnable(bitmapTaskHandler,wallpaperManager);
                executorService.execute(wallpaperSettingRunnable);
            } else if (resultCode == RESULT_CANCELED){
                inProgress = false;
            }
            else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                inProgress = false;
            }

        }
    }

    @Override
    public boolean isInProgress() {
        return inProgress;
    }

    @Override
    public void handleState(BitmapTaskHandler taskHandler, int state) {
        Message completeMessage = handler.obtainMessage(state,taskHandler);
        completeMessage.sendToTarget();
    }
}