package com.eudox.background.backgroundchanger.fragment.dialog

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.eudox.background.backgroundchanger.persist.AppData
import java.util.*

class DatePickerDialogFragment() : DialogFragment() {
    companion object{
        const val INIT_DATE_KEY = "INIT_DATE"
        const val LISTENER_SELECT_KEY = "LISTENER_SELECT"
    }

    private lateinit var onStartDateSetListener: DatePickerDialog.OnDateSetListener
    private lateinit var onEndDateSetListener: DatePickerDialog.OnDateSetListener

    interface RandomDateDialogListenerProvider{
        fun getOnStartDateSetListener(): DatePickerDialog.OnDateSetListener
        fun getOnEndDateSetListener(): DatePickerDialog.OnDateSetListener
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val dateSetListenerProvider = targetFragment as? RandomDateDialogListenerProvider
        if (dateSetListenerProvider != null){
            onStartDateSetListener = dateSetListenerProvider.getOnStartDateSetListener()
            onEndDateSetListener = dateSetListenerProvider.getOnEndDateSetListener()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // Use the current date as the default date in the picker
        val c : Calendar = Calendar.getInstance()
        val initDate = arguments?.getLong(INIT_DATE_KEY)
        val listenerSelect = arguments?.getString(LISTENER_SELECT_KEY)
        if (initDate != null){
            c.time = Date(initDate)
        }
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        var listener: DatePickerDialog.OnDateSetListener? = null

        when(listenerSelect){
            AppData.PAR_RAND_START_DTE -> {
                listener = onStartDateSetListener
            }
            AppData.PAR_RAND_END_DTE -> {
                listener = onEndDateSetListener
            }
        }

        return DatePickerDialog(requireContext(),listener,year,month,day)
    }
}