package com.eudox.background.backgroundchanger.fragment.dialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.eudox.background.backgroundchanger.R


class TimePeriodDialogFragment() : DialogFragment() {

    private lateinit var timeDialogInit: (timeView: View) -> Unit
    private lateinit var positiveButtonClick: (dialog: DialogInterface, timeView: View) -> Unit
    private lateinit var negativeButtonClick: (dialog: DialogInterface, timeView: View) -> Unit

    fun interface InitTimeViewProvider{
        fun getTimeDialogInit() : (timeView: View) -> Unit
    }

    interface TimePeriodDialogListenerProvider{
        fun getOnPositiveButtonClickListener(): (dialog: DialogInterface, timeView: View) -> Unit
        fun getOnNegativeButtonClickListener(): (dialog: DialogInterface, timeView: View) -> Unit
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val initTimeViewProvider = targetFragment as? InitTimeViewProvider
        val timePeriodDialogListenerProvider = targetFragment as? TimePeriodDialogListenerProvider

        if (initTimeViewProvider != null){
            timeDialogInit = initTimeViewProvider.getTimeDialogInit()
        }
        if (timePeriodDialogListenerProvider != null){
            positiveButtonClick = timePeriodDialogListenerProvider.getOnPositiveButtonClickListener()
            negativeButtonClick = timePeriodDialogListenerProvider.getOnNegativeButtonClickListener()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        return activity?.let {
            // Use the Builder class for convenient dialog construction
            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater
            val timeView = inflater.inflate(R.layout.dialog_period_picker,null)

            timeDialogInit.invoke(timeView)

            builder.setView(timeView)
                    .setPositiveButton("Accept", DialogInterface.OnClickListener { dialog, id ->
                        positiveButtonClick.invoke(dialog,timeView)
                    })
                    .setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, id ->
                        negativeButtonClick.invoke(dialog,timeView)
                    })
                    .setTitle(R.string.time_unit_dialog_title)
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

}
