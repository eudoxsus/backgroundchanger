package com.eudox.background.backgroundchanger.persist

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.eudox.background.backgroundchanger.adapter.AdapterPhoto
import java.util.*

@Entity(tableName = Picture.TABLE_NAME, primaryKeys = arrayOf(Picture.ID_COL, Picture.ALBUM_ID_COL))
data class Picture(@ColumnInfo(name = ID_COL) override val id: String,
                   @ColumnInfo(name = ALBUM_ID_COL) val album_id: String,
                   override val source: String,
                   val enabled: Boolean,
                   val favorite: Boolean,
                   val create_date: Date?,
                   val last_use_date: Date?
                   ) : AdapterPhoto {
    companion object{
        const val TABLE_NAME = "picture_table"
        const val ID_COL = "picture_id"
        const val ALBUM_ID_COL = "album_id"
        const val LAST_USE_DATE_COL = "last_use_date"
    }

    override fun equals(obj: Any?): Boolean {
        if (obj is AdapterPhoto) {
            val cast = obj
            return source == cast.source && id == cast.id
        }
        return false
    }
}