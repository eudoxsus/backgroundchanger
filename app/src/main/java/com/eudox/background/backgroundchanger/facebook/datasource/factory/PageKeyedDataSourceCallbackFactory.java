package com.eudox.background.backgroundchanger.facebook.datasource.factory;

import com.facebook.GraphRequest;

import androidx.paging.PageKeyedDataSource;

public interface PageKeyedDataSourceCallbackFactory<T,U> {

    GraphRequest.Callback initialCallback(final PageKeyedDataSource.LoadInitialCallback<T, U> initCallback);

    GraphRequest.Callback beforeCallback(final PageKeyedDataSource.LoadCallback<T, U> befCallback);

    GraphRequest.Callback afterCallback(final PageKeyedDataSource.LoadCallback<T, U> aftCallback);

}
