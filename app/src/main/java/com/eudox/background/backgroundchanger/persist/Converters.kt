package com.eudox.background.backgroundchanger.persist

import androidx.room.TypeConverter
import java.util.*
import java.util.concurrent.TimeUnit

class Converters {

    inline fun <reified T : Enum<*>> enumValueOrNull(name: String): T? =
            T::class.java.enumConstants?.firstOrNull { it.name == name }

    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time?.toLong()
    }

    @TypeConverter
    fun fromTimeUnit(value: String?): TimeUnit? {
        return value?.let { enumValueOrNull(value) }
    }

    @TypeConverter
    fun timeUnitToString(timeUnit: TimeUnit?): String? {
        return timeUnit?.name
    }
}