package com.eudox.background.backgroundchanger.viewmodel.enums

enum class MultiAlbumModeEnum {
    FACEBOOK,
    LOCAL
}