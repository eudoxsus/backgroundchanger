package com.eudox.background.backgroundchanger.adapter

import android.widget.CheckBox

interface OnSelectBoxClickListener {
    fun onClick(checkBox: CheckBox,album: AdapterAlbum)
}