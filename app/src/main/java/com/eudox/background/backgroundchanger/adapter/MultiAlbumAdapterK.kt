package com.eudox.background.backgroundchanger.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eudox.background.backgroundchanger.R
import com.eudox.background.backgroundchanger.viewmodel.MultiAlbumViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.apache.commons.lang3.StringUtils


class MultiAlbumAdapterK (private val context: Context, private val itemViewListener: OnAlbumItemClickListener,
                         private val checkboxListener: OnSelectBoxClickListener,
                         private val isSelected: IsSelected,
                         diffCallback: AdapterAlbumDiffCallback<AdapterAlbum>) : PagedListAdapter<AdapterAlbum, MultiAlbumAdapterK.ViewHolder>(diffCallback) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val album = getItem(position)
        if (album != null) {
            holder.bindTo(album)
        } else {
            Toast.makeText(context, "Item is null", Toast.LENGTH_LONG).show()
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val imageView: ImageView
        private val textView: TextView
        private val checkBox: CheckBox

        fun bindTo(album: AdapterAlbum) {
            val albumTitle = if (StringUtils.isBlank(album.name)) "Untitled Album" else album.name!!

            //Set Album title
            textView.text = albumTitle

            //Set image
            Glide.with(context).asBitmap()
                    .load(album.coverImage)
                    .into(imageView)

            //TODO Refactor, I dont like this
            CoroutineScope(Dispatchers.Main).launch {
                checkBox.isChecked = isSelected.isSelected(album)
                if (checkBox.isChecked){
                    checkBox.text = MultiAlbumViewModel.ENABLED_TXT
                }
                else{
                    checkBox.text = MultiAlbumViewModel.DISABLED_TXT
                }
            }


            if (!album.isEmpty) {
                checkBox.visibility = View.VISIBLE
                checkBox.setOnClickListener { checkboxListener.onClick(checkBox, album) }
                itemView.setOnClickListener { itemViewListener.onClick(itemView, album) }
            } else {
                checkBox.visibility = View.INVISIBLE
            }
        }

        init {
            imageView = itemView.findViewById(R.id.cardImage)
            textView = itemView.findViewById(R.id.cardText)
            checkBox = itemView.findViewById(R.id.selectBox)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.album_list_cell, parent, false)
        return ViewHolder(view)
    }


}