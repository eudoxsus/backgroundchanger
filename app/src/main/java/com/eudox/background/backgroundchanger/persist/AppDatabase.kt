package com.eudox.background.backgroundchanger.persist

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = arrayOf(Picture::class,Album::class,AppData::class), version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase: RoomDatabase() {

    abstract fun pictureDao(): PictureDao

    abstract fun albumDao(): AlbumDao

    abstract fun appDataDao(): AppDataDao

    companion object{

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase{
            val tempInstance = INSTANCE
            if (tempInstance != null){
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(context,AppDatabase::class.java,"app_database").build()
                INSTANCE = instance
                return instance
            }
        }
    }

}