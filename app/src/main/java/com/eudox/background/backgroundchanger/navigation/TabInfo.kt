package com.eudox.background.backgroundchanger.navigation

import androidx.fragment.app.Fragment

interface TabInfo {
    fun getTabTitle(): String
    fun createFragment(): Fragment
}