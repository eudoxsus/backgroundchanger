package com.eudox.background.backgroundchanger.persist

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = AppData.TABLE_NAME, primaryKeys = arrayOf(AppData.APP_ID_COL,AppData.PARAM_ID_COL))
data class AppData(@ColumnInfo(name = APP_ID_COL) val appId: String, @ColumnInfo(name = PARAM_ID_COL) val paramId: String, val value: String) {
    companion object{
        const val TABLE_NAME = "appData_table"
        const val APP_ID_COL = "appId"
        const val PARAM_ID_COL = "paramId"

        const val APP_ID_RAND = "RANDOM"
        const val PAR_RAND_END_DTE = "randEndDate"
        const val PAR_RAND_START_DTE = "randStartDate"

        const val APP_ID_WORK = "WORK_PERIOD"
        const val PAR_WP_TIME_VAL = "value"
        const val PAR_WP_TU = "timeUnit"
        const val PAR_WP_SETTER = "setterFlag"
        const val PAR_WP_NEXT_WORK_DTE = "next_work_date"

    }

    override fun equals(other: Any?): Boolean {
        return if (other is AppData){
            val otherAppData: AppData = other
            if (this.appId == otherAppData.appId){
                if (this.paramId == otherAppData.paramId){
                    this.value == otherAppData.value
                } else{
                    false
                }
            } else{
                false
            }
        }
        else{
            false
        }
    }
}