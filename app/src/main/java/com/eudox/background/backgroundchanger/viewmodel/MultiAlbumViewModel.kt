package com.eudox.background.backgroundchanger.viewmodel

import android.app.Application
import android.view.View
import android.widget.CheckBox
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.eudox.background.backgroundchanger.adapter.AdapterAlbum
import com.eudox.background.backgroundchanger.adapter.IsSelected
import com.eudox.background.backgroundchanger.adapter.OnAlbumItemClickListener
import com.eudox.background.backgroundchanger.adapter.OnSelectBoxClickListener
import com.eudox.background.backgroundchanger.facebook.datasource.factory.MultiAlbumDataSourceFactory
import com.eudox.background.backgroundchanger.persist.Album
import com.eudox.background.backgroundchanger.persist.AlbumDao
import com.eudox.background.backgroundchanger.persist.AppDatabase
import com.facebook.AccessToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MultiAlbumViewModel(application: Application) : AndroidViewModel(application), OnAlbumItemClickListener, OnSelectBoxClickListener, IsSelected {
    companion object{
        val ENABLED_TXT = "Enabled"
        val DISABLED_TXT = "Disabled"
    }


    val albumPagedList: LiveData<PagedList<AdapterAlbum>>
    //val liveDataSource: LiveData<PageKeyedDataSource<String, AlbumJson>>
    val allAlbums: LiveData<List<Album>>

    private val albumDao: AlbumDao = AppDatabase.getDatabase(application).albumDao()

    init {
        allAlbums = albumDao.getAllAlbumsLive()
        val multiAlbumDataSourceFactory = MultiAlbumDataSourceFactory()
        //liveDataSource = multiAlbumDataSourceFactory.multiAlbumLiveDataSource
        val config = PagedList.Config.Builder().setEnablePlaceholders(false).setPageSize(20).build()
        albumPagedList = LivePagedListBuilder(multiAlbumDataSourceFactory, config).build()
    }

    //Album to SingleAlbum Fragment
    override fun onClick(view: View, album: AdapterAlbum) {
        Toast.makeText(view.context,"Dummy action to single album view",Toast.LENGTH_SHORT).show()
    }

    //On Checkbox
    override fun onClick(checkBox: CheckBox, album: AdapterAlbum) {
        viewModelScope.launch(Dispatchers.IO) {
            //Disable this selectbox from changing state until the operation is done
            if (checkBox.isEnabled){
                withContext(Dispatchers.Main){
                    checkBox.isEnabled = false
                }
                val albumDb: Album? = albumDao.getAlbumById(album.id)
                val id = AccessToken.getCurrentAccessToken().userId
                if (checkBox.isChecked){
                    if (albumDb != null){
                        albumDao.update(Album(album.id,id,true))
                    }
                    else{
                        albumDao.insert(Album(album.id,id,true))
                    }
                    withContext(Dispatchers.Main){
                        checkBox.text = ENABLED_TXT
                    }
                }
                else{
                    if (albumDb != null){
                        albumDao.update(Album(album.id,id,false))
                    }
                    withContext(Dispatchers.Main){
                        checkBox.text = DISABLED_TXT
                    }
                }
                withContext(Dispatchers.Main){
                    checkBox.isEnabled = true
                }
            }
            else{
                Toast.makeText(checkBox.context,"Wait for operation to finish",Toast.LENGTH_SHORT).show()
            }
        }

    }

    override suspend fun isSelected(album: AdapterAlbum): Boolean {
        val albumDb = albumDao.getAlbumById(album.id)
        if (albumDb != null){
            return albumDb.enabled
        }
        return false
    }
}