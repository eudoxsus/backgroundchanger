package com.eudox.background.backgroundchanger.facebook.model;

import com.eudox.background.backgroundchanger.adapter.AdapterPhoto;

import androidx.annotation.Nullable;

public class PhotoData extends FacebookObj implements AdapterPhoto {
    protected String source;
    protected String picture = "";

    public String getPicture() {
        return picture;
    }

    public String getSource() {
        return source;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if(obj instanceof PhotoData){
            PhotoData cast = (PhotoData) obj;
            return source.equals(cast.getSource()) && id.equals(cast.getId());
        }
        return false;
    }
}
