package com.eudox.background.backgroundchanger.facebook.random

import com.facebook.GraphRequest

interface Randomizer : GraphRequest.Callback {
    suspend fun randomWallpaperWork() : RandomizerResult
    fun interface OnCompleteListener{
        fun onComplete()
    }
}