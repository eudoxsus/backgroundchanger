package com.eudox.background.backgroundchanger.persist

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface AlbumDao {

    @Query("SELECT * FROM " + Album.TABLE_NAME + " WHERE "+ Album.ID_COL + " = :id")
    suspend fun getAlbumById(id : String): Album?

    @Query("DELETE FROM " + Album.TABLE_NAME + " WHERE " + Album.ID_COL + " = :id")
    suspend fun deleteAlbumById(id : String): Int

    /*SELECT * FROM table WHERE id IN (SELECT id FROM table ORDER BY RANDOM() LIMIT x)*/
    @Query("SELECT * FROM " + Album.TABLE_NAME + " WHERE " + Album.ID_COL + " IN (SELECT "+ Album.ID_COL +" FROM " + Album.TABLE_NAME + " ORDER BY RANDOM() LIMIT 1)")
    suspend fun getRandomAlbum(): Album

    @Query("SELECT * FROM " + Album.TABLE_NAME)
    suspend fun getAllAlbums(): List<Album>

    @Query("SELECT * FROM " + Album.TABLE_NAME)
    fun getAllAlbumsLive(): LiveData<List<Album>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(album: Album)

    @Update
    suspend fun update(album: Album)

}