package com.eudox.background.backgroundchanger.async.deprecated;

import android.app.WallpaperManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;


public class WallpaperSettingRunnable implements Runnable {

    private WallpaperManager wallpaperManager;
    private BitmapTaskHandler handler;

    public WallpaperSettingRunnable(BitmapTaskHandler handler,WallpaperManager wallpaperManager){
        this.handler = handler;
        this.wallpaperManager = wallpaperManager;
    }

    @Override
    public void run() {
        try {
            Bitmap bm = BitmapFactory.decodeFile(handler.getImgFile().getPath());
            wallpaperManager.setBitmap(bm);
            handler.handleState(BitmapTaskHandler.WALLPAPER_SET_COMPLETED);
        } catch (IOException e) {
            handler.handleState(BitmapTaskHandler.RUNNABLE_ERR);
            e.printStackTrace();
        }
    }
}
