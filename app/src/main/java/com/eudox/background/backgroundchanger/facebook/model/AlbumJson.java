package com.eudox.background.backgroundchanger.facebook.model;

import com.eudox.background.backgroundchanger.R;
import com.eudox.background.backgroundchanger.adapter.AdapterAlbum;

public class AlbumJson extends FacebookObj implements AdapterAlbum {

    protected PhotosContainer photos;

    public PhotosContainer getPhotos() {
        return photos;
    }

    public void setPhotos(PhotosContainer photos) {
        this.photos = photos;
    }

    @Override
    public String getCoverImage() {
        if (!isEmpty()){
            return getPhotos().getData().get(0).getSource();
        }
        else{
            return String.valueOf(R.drawable.ic_launcher_background);
        }
    }

    @Override
    public boolean isEmpty() {
        return getPhotos() == null;
    }
}
