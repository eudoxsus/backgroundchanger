package com.eudox.background.backgroundchanger.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.eudox.background.backgroundchanger.R
import com.eudox.background.backgroundchanger.adapter.SettingsPageAdapter
import com.eudox.background.backgroundchanger.navigation.SettingsTabInfoHolder
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingsFragment : Fragment() {

    private lateinit var settingsPageAdapter: SettingsPageAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (requireActivity() as AppCompatActivity).supportActionBar?.title = ""

        val tabInfoHolder = SettingsTabInfoHolder()

        settingsPageAdapter = SettingsPageAdapter(this, tabInfoHolder.tabInfos)

        viewPager.adapter = settingsPageAdapter

        TabLayoutMediator(tabLayout, viewPager,
            TabLayoutMediator.TabConfigurationStrategy { tab, position ->
                tab.text = tabInfoHolder.tabInfos[position].getTabTitle()
            }).attach()
    }

}