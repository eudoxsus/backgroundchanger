package com.eudox.background.backgroundchanger.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.eudox.background.backgroundchanger.R;

import org.apache.commons.lang3.StringUtils;


import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

public class MultiAlbumAdapter extends PagedListAdapter<AdapterAlbum, MultiAlbumAdapter.ViewHolder>  {

    private Context context;
    private OnSelectBoxClickListener selectBoxClickListener;
    private OnAlbumItemClickListener itemViewListener;
    private IsSelected isSelected;

    public MultiAlbumAdapter(Context context,OnAlbumItemClickListener itemViewListener,OnSelectBoxClickListener checkboxListener,IsSelected isSelected) {
        super(DIFF_CALLBACK);
        this.context = context;
        this.selectBoxClickListener = checkboxListener;
        this.itemViewListener = itemViewListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private TextView textView;
        private CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.cardImage);
            textView = itemView.findViewById(R.id.cardText);
            checkBox = itemView.findViewById(R.id.selectBox);
        }

        public void bindTo(final AdapterAlbum album){
            String albumTitle = StringUtils.isBlank(album.getName()) ? "Untitled Album" : album.getName();

            //Set Album title
            textView.setText(albumTitle);

            //Set image
            Glide.with(context).asBitmap()
                    .load(album.getCoverImage())
                    .into(imageView);

            //TODO update checked status on update in the observer
            //checkBox.setChecked(isSelected.isSelected(album));

            if (!album.isEmpty()){
                checkBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectBoxClickListener.onClick((CheckBox) checkBox,album);
                    }
                });
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        itemViewListener.onClick(itemView,album);
                    }
                });
            }
            else{
                checkBox.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.album_list_cell,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        AdapterAlbum album = getItem(position);
        if (album != null){
            holder.bindTo(album);
        }
        else{
            Toast.makeText(context,"Item is null",Toast.LENGTH_LONG).show();
        }
    }

    private static DiffUtil.ItemCallback<AdapterAlbum> DIFF_CALLBACK = new DiffUtil.ItemCallback<AdapterAlbum>() {
        @Override
        public boolean areItemsTheSame(@NonNull AdapterAlbum oldItem, @NonNull AdapterAlbum newItem) {
            return oldItem.getId().equals(newItem.getId());
        }

        @Override
        public boolean areContentsTheSame(@NonNull AdapterAlbum oldItem, @NonNull AdapterAlbum newItem) {
            return oldItem.equals(newItem);
        }
    };
}
