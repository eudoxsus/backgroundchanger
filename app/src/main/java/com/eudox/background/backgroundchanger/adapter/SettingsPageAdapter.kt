package com.eudox.background.backgroundchanger.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.eudox.background.backgroundchanger.navigation.TabInfo

class SettingsPageAdapter(fragment: Fragment, private val tabInfos: List<TabInfo>) : FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int {
        return tabInfos.size
    }

    override fun createFragment(position: Int): Fragment {
        return tabInfos[position].createFragment()
    }

}