package com.eudox.background.backgroundchanger.persist

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.*

@Dao
interface PictureDao {

    @Query("SELECT * FROM " + Picture.TABLE_NAME + " WHERE " + Picture.ID_COL +" = :id")
    suspend fun getPictureByPictureId(id : String): Picture?

    @Query("SELECT * FROM " + Picture.TABLE_NAME + " WHERE " + Picture.ID_COL +" = :id AND " + Picture.ALBUM_ID_COL + " = :albumId")
    suspend fun getPictureByFullId(id : String,albumId: String): Picture?

    @Query("DELETE FROM " + Picture.TABLE_NAME + " WHERE " + Picture.ID_COL +" = :id")
    suspend fun deletePictureById(id : String): Int

    @Query("SELECT * FROM " + Picture.TABLE_NAME + " WHERE " + Picture.ID_COL + " IN (SELECT " + Picture.ID_COL +" FROM " + Picture.TABLE_NAME + " ORDER BY RANDOM() LIMIT 1)")
    suspend fun getRandomPicture(): Picture

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(picture: Picture)

    @Update
    suspend fun update(picture: Picture)

    /**
     * DataSource.Factory used for PagedList History
     */
    @Query("SELECT * FROM  " + Picture.TABLE_NAME + " ORDER BY " + Picture.LAST_USE_DATE_COL + " DESC")
    fun getPagedPicturesByLastUsedDate(): DataSource.Factory<Int, Picture>

    /**
     * LiveData meant for Select Page UI.
     */
    @Query("SELECT * FROM  " + Picture.TABLE_NAME + " ORDER BY " + Picture.LAST_USE_DATE_COL + " DESC LIMIT :limit")
    fun getLivePicturesByLastUsedDate(limit : Int = 999): LiveData<List<Picture>>
}