package com.eudox.background.backgroundchanger.adapter

import androidx.recyclerview.widget.DiffUtil

class AdapterPhotoDiffCallback<T : AdapterPhoto> : DiffUtil.ItemCallback<T>() {

    override fun areItemsTheSame(oldItem: T, newItem: T): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: T, newItem: T): Boolean {
        return oldItem == newItem
    }

}