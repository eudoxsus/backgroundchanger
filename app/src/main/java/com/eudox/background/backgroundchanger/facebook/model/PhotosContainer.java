package com.eudox.background.backgroundchanger.facebook.model;

import java.util.List;

public class PhotosContainer {

    private List<PhotoData> data;
    private Paging paging;

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    public List<PhotoData> getData() {
        return data;
    }

    public void setData(List<PhotoData> data) {
        this.data = data;
    }
}
