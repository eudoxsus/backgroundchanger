package com.eudox.background.backgroundchanger.viewmodel;

import android.app.Application;

import com.eudox.background.backgroundchanger.facebook.datasource.factory.SinglePhotoDataSourceFactory;
import com.eudox.background.backgroundchanger.facebook.model.PhotoData;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

public class SingleAlbumViewModel extends AndroidViewModel {

    public LiveData<PagedList<PhotoData>> photoDataPagedList;

    public SingleAlbumViewModel(Application application, String albumId){
        super(application);

        //liveDataSource = singlePhotoDataSourceFactory.getSingleAlbumLiveDataSource();

        SinglePhotoDataSourceFactory singlePhotoDataSourceFactory = new SinglePhotoDataSourceFactory(albumId);
        PagedList.Config config = new PagedList.Config.Builder().setEnablePlaceholders(false).setPageSize(20).build();
        photoDataPagedList = new LivePagedListBuilder(singlePhotoDataSourceFactory,config).build();
    }


}
