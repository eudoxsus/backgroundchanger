package com.eudox.background.backgroundchanger.async.deprecated;

public interface BitmapManager {
    int DECODE_COMPLETE = 1479875;

    int WALLPAPER_SET_COMPLETE = 112277;

    int INPROGRESS_SUBMISSION = 879997;

    int RUNNABLE_START = 111111;

    int RUNNABLE_ERR = -111111;

    boolean isInProgress();

    void handleState(BitmapTaskHandler taskHandler,int state);
}
